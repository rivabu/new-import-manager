<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="no"/>

    <!-- Oneliner for better performance -->
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- PDF filter -->
    <xsl:template match="/univentio-patent-document">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
            <xsl:element name="PDF">
                <xsl:choose>
                    <xsl:when test="Images/PDFs/Image/@FileSize > 1">
                        <xsl:text>true</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>false</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
        </xsl:copy>
    </xsl:template>

    <!-- @ChangeDate for monitoring of (partial) updates to documents -->
    <xsl:template match="//@ChangeDate">
        <xsl:attribute name="ChangeDate">
            <xsl:call-template name="formatDate">
                <xsl:with-param name="dateStr" select="normalize-space(.)"/>
            </xsl:call-template>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="/univentio-patent-document/@InsertDate">
        <xsl:attribute name="InsertDate">
            <xsl:call-template name="formatDate">
                <xsl:with-param name="dateStr" select="normalize-space(/univentio-patent-document/@InsertDate)"/>
            </xsl:call-template>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="/univentio-patent-document/Publication">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
            <!-- <authority + kind> -->
            <xsl:element name="PIDV">
                <xsl:value-of select="Authority/@Code"/>
                <xsl:value-of select="Number"/>
            </xsl:element>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="Titles">
        <xsl:element name="{name(.)}">
            <xsl:element name="title">
                <xsl:for-each select="Title">
                    <xsl:variable name="lang" select="@Lang_ISO639-2"/>
                    <xsl:choose>
                        <!--CN documents: original value should be selected as the Chinese language field instead of OCR-->
                        <xsl:when test="/univentio-patent-document/Publication/Authority[@Code='CN']">
                            <xsl:if test="@DataType='Xml' or @DataType='Mat'">
                                <xsl:element name="entry">
                                    <xsl:element name="key">
                                        <xsl:value-of select="@Lang_ISO639-2"/>
                                    </xsl:element>
                                    <xsl:element name="value">
                                        <xsl:attribute name="DataType">
                                            <xsl:value-of select="@DataType"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="normalize-space(.)"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:if>
                            <xsl:if test="@DataType='Ocr' and not(parent::*/*[@DataType='Xml' and @Lang_ISO639-2=$lang])">
                                <xsl:element name="entry">
                                    <xsl:element name="key">
                                        <xsl:value-of select="@Lang_ISO639-2"/>
                                    </xsl:element>
                                    <xsl:element name="value">
                                        <xsl:attribute name="DataType">
                                            <xsl:value-of select="@DataType"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="normalize-space(.)"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:if>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="entry">
                                <xsl:element name="key">
                                    <xsl:value-of select="@Lang_ISO639-2"/>
                                </xsl:element>
                                <xsl:element name="value">
                                    <xsl:value-of select="normalize-space(.)"/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:element>
        </xsl:element>
        <!--
        <xsl:apply-templates select=" node()"/>
        -->

    </xsl:template>


    <xsl:template match="Abstracts">
        <xsl:element name="{name(.)}">
            <xsl:element name="documentAbstract">
                <xsl:for-each select="Abstract">
                    <xsl:variable name="lang" select="@Lang_ISO639-2"/>
                    <xsl:choose>
                        <!--CN documents: original value should be selected as the Chinese language field instead of OCR-->
                        <xsl:when test="/univentio-patent-document/Publication/Authority[@Code='CN']">
                            <xsl:if test="@DataType='Xml' or @DataType='Mat'">
                                <xsl:element name="entry">
                                    <xsl:element name="key">
                                        <xsl:value-of select="$lang"/>
                                    </xsl:element>
                                    <xsl:element name="value">
                                        <xsl:attribute name="DataType">
                                            <xsl:value-of select="@DataType"/>
                                        </xsl:attribute>
                                        <xsl:element name="Text">
                                            <xsl:value-of select="normalize-space(.)"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:if>
                            <xsl:if test="@DataType='Ocr' and not(parent::*/*[@DataType='Xml' and @Lang_ISO639-2=$lang])">
                                <xsl:element name="entry">
                                    <xsl:element name="key">
                                        <xsl:value-of select="$lang"/>
                                    </xsl:element>
                                    <xsl:element name="value">
                                        <xsl:attribute name="DataType">
                                            <xsl:value-of select="@DataType"/>
                                        </xsl:attribute>
                                        <xsl:element name="Text">
                                            <xsl:value-of select="normalize-space(.)"/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:if>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="entry">
                                <xsl:element name="key">
                                    <xsl:value-of select="$lang"/>
                                </xsl:element>
                                <xsl:element name="value">
                                    <xsl:attribute name="DataType">
                                        <xsl:value-of select="@DataType"/>
                                    </xsl:attribute>
                                    <xsl:element name="Text">
                                        <xsl:value-of select="normalize-space(.)"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:element>
        </xsl:element>
        <!--
        <xsl:apply-templates select=" node()"/>
        -->
    </xsl:template>

    <xsl:template match="Descriptions">
        <xsl:element name="{name(.)}">
            <xsl:element name="description">
                <xsl:for-each select="Description">
                    <xsl:variable name="lang" select="@Lang_ISO639-2"/>
                    <xsl:choose>
                        <!--CN documents: original value should be selected as the Chinese language field instead of OCR-->
                        <xsl:when test="/univentio-patent-document/Publication/Authority[@Code='CN']">
                            <xsl:if test="@DataType='Xml' or @DataType='Mat'">
                                <xsl:element name="entry">
                                    <xsl:element name="key">
                                        <xsl:value-of select="$lang"/>
                                    </xsl:element>
                                    <xsl:element name="value">
                                        <xsl:attribute name="DataType">
                                            <xsl:value-of select="@DataType"/>
                                        </xsl:attribute>
                                        <xsl:call-template name="descriptionsSubElements">
                                            <xsl:with-param name="incomingElement">brfsum</xsl:with-param>
                                        </xsl:call-template>
                                        <xsl:call-template name="descriptionsSubElements">
                                            <xsl:with-param name="incomingElement">detdesc</xsl:with-param>
                                        </xsl:call-template>
                                        <xsl:call-template name="descriptionsSubElements">
                                            <xsl:with-param name="incomingElement">drwdesc</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="descriptionsSubElementsText"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:if>
                            <xsl:if test="@DataType='Ocr' and not(parent::*/*[@DataType='Xml' and @Lang_ISO639-2=$lang])">
                                <xsl:element name="entry">
                                    <xsl:element name="key">
                                        <xsl:value-of select="$lang"/>
                                    </xsl:element>
                                    <xsl:element name="value">
                                        <xsl:attribute name="DataType">
                                            <xsl:value-of select="@DataType"/>
                                        </xsl:attribute>
                                        <xsl:call-template name="descriptionsSubElements">
                                            <xsl:with-param name="incomingElement">brfsum</xsl:with-param>
                                        </xsl:call-template>
                                        <xsl:call-template name="descriptionsSubElements">
                                            <xsl:with-param name="incomingElement">detdesc</xsl:with-param>
                                        </xsl:call-template>
                                        <xsl:call-template name="descriptionsSubElements">
                                            <xsl:with-param name="incomingElement">drwdesc</xsl:with-param>
                                        </xsl:call-template>

                                        <xsl:call-template name="descriptionsSubElementsText"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:if>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="entry">
                                <xsl:element name="key">
                                    <xsl:value-of select="$lang"/>
                                </xsl:element>
                                <xsl:element name="value">
                                    <xsl:attribute name="DataType">
                                        <xsl:value-of select="@DataType"/>
                                    </xsl:attribute>
                                    <xsl:call-template name="descriptionsSubElements">
                                        <xsl:with-param name="incomingElement">brfsum</xsl:with-param>
                                    </xsl:call-template>
                                    <xsl:call-template name="descriptionsSubElements">
                                        <xsl:with-param name="incomingElement">detdesc</xsl:with-param>
                                    </xsl:call-template>
                                    <xsl:call-template name="descriptionsSubElements">
                                        <xsl:with-param name="incomingElement">drwdesc</xsl:with-param>
                                    </xsl:call-template>

                                    <xsl:call-template name="descriptionsSubElementsText"/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:element>
        </xsl:element>
        <!--
        <xsl:apply-templates select=" node()"/>
        -->
    </xsl:template>

    <xsl:template name="descriptionsSubElements">
        <xsl:param name="incomingElement"/>
        <xsl:element name="{$incomingElement}">
            <xsl:variable name="val">
                <xsl:value-of select="./*[name()=$incomingElement]" separator=" "/>
            </xsl:variable>
            <xsl:value-of select="normalize-space($val)"/>
        </xsl:element>
    </xsl:template>
    <xsl:template name="descriptionsSubElementsText">
        <xsl:element name="Text">
            <xsl:variable name="val">
                <xsl:value-of select="./*[not(self::brfsum or self::detdesc or self::drwdesc)]" separator=" "/>
            </xsl:variable>
            <xsl:value-of select="normalize-space($val)"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="Amendments/Amendment/AmendmentGroup/Content/claims">
        <xsl:element name="{name(.)}">
            <xsl:copy-of select="@*"/>
            <xsl:if test="claim[@num='1']">
                <xsl:element name="claim1">
                    <entry>
                        <key>
                            <xsl:value-of select="../../@Lang_ISO639-2"/>
                        </key>
                        <value>
                            <xsl:for-each select="claim[@num='1']/claim-text">
                                <xsl:value-of select="normalize-space(.)" separator=" "/>
                            </xsl:for-each>
                        </value>
                    </entry>
                </xsl:element>
            </xsl:if>

            <xsl:if test="claim[not(@num=1)]">
                <xsl:element name="claim">
                    <entry>
                        <key>
                            <xsl:value-of select="../../@Lang_ISO639-2"/>
                        </key>
                        <value>
                            <xsl:for-each select="claim[not(@num='1')]/claim-text">
                                <xsl:value-of select="normalize-space(.)"/>
                            </xsl:for-each>
                        </value>
                    </entry>
                </xsl:element>
            </xsl:if>
        </xsl:element>
        <!--
        <xsl:apply-templates select=" node()"/>
        -->
    </xsl:template>

    <xsl:template match="NationalNotifications/NationalNotification">
        <xsl:element name="NationalNotification">
            <xsl:attribute name="Lang_ISO639-2">
                <xsl:value-of select="@Lang_ISO639-2"/>
            </xsl:attribute>
            <xsl:if test="Notification/@Description">
                <xsl:element name="Description">
                    <xsl:value-of select="Notification/@Description" separator=" "/>
                </xsl:element>
            </xsl:if>
        </xsl:element>
        <!--
        <xsl:apply-templates select=" node()"/>
        -->
    </xsl:template>

    <xsl:template
            match="PostIssuanceData/DailyAssignments/DailyAssignment/Assignees/Assignee/Extended | PostIssuanceData/DailyAssignments/DailyAssignment/Assignors/Assignor/Extended">
        <xsl:element name="Extended">
            <xsl:element name="Name">
                <xsl:value-of select="normalize-space(.)" separator=" "/>
            </xsl:element>
        </xsl:element>
        <!--
        Enabling following instruction adds word "norm" to the model from this element <Name Type="normalized" DCA="123" Key="123">CLANCEY, STEPHEN M. norm</Name>
        <xsl:apply-templates select=" node()"/>
        -->
    </xsl:template>


    <xsl:key name="language" match="Claims" use="@Lang_ISO639-2"/>
    <xsl:template match="Claims[Claims]">
        <Claims>
            <claim>
                <xsl:for-each select="Claims[generate-id(.)=generate-id(key('language',@Lang_ISO639-2)[1])]">
                    <xsl:variable name="language" select="@Lang_ISO639-2"/>
                    <xsl:choose>
                        <!--CN documents: original value should be selected as the Chinese language field instead of OCR-->
                        <xsl:when test="/univentio-patent-document/Publication/Authority[@Code='CN']">
                            <xsl:if test="@DataType='Xml' or @DataType='Mat'">
                                <entry>
                                    <key>
                                        <xsl:value-of select="$language"/>
                                    </key>
                                    <value>
                                        <xsl:for-each select="../Claims[@Lang_ISO639-2 = $language]/Claim">
                                            <ClaimText>
                                                <xsl:attribute name="DataType">
                                                    <xsl:value-of select="../@DataType"/>
                                                </xsl:attribute>
                                                <Text>
                                                    <xsl:value-of select="normalize-space(.)"/>
                                                </Text>
                                                <xsl:if test="@ClaimNo='1'">
                                                    <ClaimType>Claim1</ClaimType>
                                                </xsl:if>
                                                <xsl:if test="@Independent='true'">
                                                    <ClaimType>Independent</ClaimType>
                                                </xsl:if>
                                                <xsl:if test="@Exemplary='true'">
                                                    <ClaimType>Exemplary</ClaimType>
                                                </xsl:if>
                                                <xsl:if test="not(@ClaimNo='1' or @Independent='true' or @Exemplary='true')">
                                                    <ClaimType>Claim</ClaimType>
                                                </xsl:if>
                                            </ClaimText>
                                        </xsl:for-each>
                                    </value>
                                </entry>
                            </xsl:if>
                            <xsl:if test="@DataType='Ocr' and not(parent::*/*[@DataType='Xml' and @Lang_ISO639-2=$language])">
                                <entry>
                                    <key>
                                        <xsl:value-of select="$language"/>
                                    </key>
                                    <value>
                                        <xsl:for-each select="../Claims[@Lang_ISO639-2 = $language]/Claim">
                                            <ClaimText>
                                                <xsl:attribute name="DataType">
                                                    <xsl:value-of select="../@DataType"/>
                                                </xsl:attribute>
                                                <Text>
                                                    <xsl:value-of select="normalize-space(.)"/>
                                                </Text>
                                                <xsl:if test="@ClaimNo='1'">
                                                    <ClaimType>Claim1</ClaimType>
                                                </xsl:if>
                                                <xsl:if test="@Independent='true'">
                                                    <ClaimType>Independent</ClaimType>
                                                </xsl:if>
                                                <xsl:if test="@Exemplary='true'">
                                                    <ClaimType>Exemplary</ClaimType>
                                                </xsl:if>
                                                <xsl:if test="not(@ClaimNo='1' or @Independent='true' or @Exemplary='true')">
                                                    <ClaimType>Claim</ClaimType>
                                                </xsl:if>
                                            </ClaimText>
                                        </xsl:for-each>
                                    </value>
                                </entry>
                            </xsl:if>
                        </xsl:when>
                        <xsl:otherwise>
                            <entry>
                                <key>
                                    <xsl:value-of select="$language"/>
                                </key>
                                <value>
                                    <xsl:for-each select="../Claims[@Lang_ISO639-2 = $language]/Claim">
                                        <ClaimText>
                                            <xsl:attribute name="DataType">
                                                <xsl:value-of select="../@DataType"/>
                                            </xsl:attribute>
                                            <Text>
                                                <xsl:value-of select="normalize-space(.)"/>
                                            </Text>
                                            <xsl:if test="@ClaimNo='1'">
                                                <ClaimType>Claim1</ClaimType>
                                            </xsl:if>
                                            <xsl:if test="@Independent='true'">
                                                <ClaimType>Independent</ClaimType>
                                            </xsl:if>
                                            <xsl:if test="@Exemplary='true'">
                                                <ClaimType>Exemplary</ClaimType>
                                            </xsl:if>
                                            <xsl:if test="not(@ClaimNo='1' or @Independent='true' or @Exemplary='true')">
                                                <ClaimType>Claim</ClaimType>
                                            </xsl:if>
                                        </ClaimText>
                                    </xsl:for-each>
                                </value>
                            </entry>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </claim>
        </Claims>
    </xsl:template>

    <xsl:template match="Corrections">
        <xsl:element name="{name(.)}">
            <xsl:text>true</xsl:text>
        </xsl:element>
    </xsl:template>

    <xsl:template match="Images/ClippedImages | Images/DrawingSheets | Images/InlineImages">
        <xsl:if test="@Count > 0">
            <xsl:element name="{name(.)}">
                <xsl:text>true</xsl:text>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template
            match="PostIssuanceData/AdverseDecisions | PostIssuanceData/CertificateOfCorrections | PostIssuanceData/Disclaimers | PostIssuanceData/ErratumReferences | PostIssuanceData/Expirations | PostIssuanceData/Reexaminations | PostIssuanceData/Reinstatements | PostIssuanceData/Reissues">
        <xsl:element name="{name(.)}">
            <xsl:text>true</xsl:text>
        </xsl:element>
    </xsl:template>

    <!-- Add current Assignee as an extra person -->
    <xsl:template match="Persons">
        <xsl:element name="{name(.)}">
            <xsl:copy-of select="@*"/>
            <xsl:for-each select="../PrimeNotifiedEvents/PrimeNotifiedSection[@Section='Ownership']/PrimeNotifiedEvent">
                <xsl:choose>
                    <xsl:when test="Date/@Type = 'to'"><!-- skip --></xsl:when>
                    <xsl:otherwise>
                        <Person>
                            <xsl:for-each select="Details[last()]/Detail">
                                <xsl:if test="not(@Type)">
                                    <CurrentAssignee>
                                        <xsl:value-of select="."/>
                                    </CurrentAssignee>
                                </xsl:if>
                                <xsl:if test="@Type = 'standardized'">
                                    <CurrentAssignee-standardized>
                                        <xsl:value-of select="."/>
                                    </CurrentAssignee-standardized>
                                </xsl:if>
                                <xsl:if test="@Type = 'normalized'">
                                    <CurrentAssignee-normalized>
                                        <xsl:value-of select="."/>
                                    </CurrentAssignee-normalized>
                                </xsl:if>
                            </xsl:for-each>
                        </Person>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <!-- Concat the person name under the <name> tag in Persons/Person -->
    <xsl:template match="Person">
        <xsl:if test="not(@DataFormat)">
            <xsl:element name="{name(.)}">
                <xsl:copy-of select="@*"/>
                <xsl:apply-templates/>
                <xsl:if test="FirstName/text() or SurName/text()">
                    <xsl:element name="Name">
                        <xsl:apply-templates select="FirstName/text()"/>
                        <xsl:text> </xsl:text>
                        <xsl:apply-templates select="SurName/text()"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test="Extended/Name[@Type = 'normalized']">
                    <xsl:element name="Name-normalized">
                        <xsl:apply-templates select="Extended/Name[@Type = 'normalized']/text()"/>
                    </xsl:element>
                </xsl:if>
                <xsl:if test="Extended/Name[@Type = 'standardized']">
                    <xsl:element name="Name-standardized">
                        <xsl:apply-templates select="Extended/Name[@Type = 'standardized']/text()"/>
                    </xsl:element>
                </xsl:if>
            </xsl:element>
        </xsl:if>
    </xsl:template>

    <xsl:template match="PrimeNotifiedEvents/PrimeNotifiedSection/PrimeNotifiedEvent/Details/Detail">
        <xsl:element name="Detail">
            <xsl:element name="Text{@Type}">
                <xsl:value-of select="normalize-space(.)"/>
            </xsl:element>
            <xsl:for-each select="../../Date[@Type = 'to']">
                <xsl:element name="Type">
                    <xsl:value-of select="normalize-space(./@Type)"/>
                </xsl:element>
            </xsl:for-each>
            <xsl:element name="PrimeNotifiedEventSection">
                <xsl:value-of select="normalize-space(../../../@Section)"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    <!-- PostIssuanceData.NoticesOfLitigation.NoticeOfLitigation.Date -->
    <xsl:template
            match="ExtendedDates/ExtendedDate | LegalEvents/LegalEvent/EffectiveDate | LegalEvents/LegalEvent/ExpiryDate | LegalEvents/LegalEvent/WithdrawnDate | Application/Date | Publication/Date | Citations/PatentCitations/PatentCitation/Date | Priorities/Priority/Date | PostIssuanceData/NoticesOfLitigation/NoticeOfLitigation/Date">
        <xsl:element name="{name(.)}">
            <xsl:call-template name="formatDate">
                <xsl:with-param name="dateStr" select="normalize-space(.)"/>
            </xsl:call-template>
        </xsl:element>
    </xsl:template>


    <!--Classification CPC-->
    <xsl:template match="Classification-CPC/Code">
        <xsl:element name="{name(.)}">
            <!-- section -->
            <xsl:value-of select="../Section/@Code"/>
            <xsl:text> </xsl:text>

            <!-- section + class -->
            <xsl:value-of select="../Section/@Code"/>
            <xsl:value-of select="../Class/@Code"/>
            <xsl:text> </xsl:text>

            <!-- section + class + subclass -->
            <xsl:value-of select="../Section/@Code"/>
            <xsl:value-of select="../Class/@Code"/>
            <xsl:value-of select="../Subclass/@Code"/>
            <xsl:text> </xsl:text>

            <!-- section + class + subclass + group -->
            <xsl:value-of select="../Section/@Code"/>
            <xsl:value-of select="../Class/@Code"/>
            <xsl:value-of select="../Subclass/@Code"/>
            <xsl:value-of select="../Group/@Code"/>
            <xsl:text> </xsl:text>

            <!-- full -->
            <xsl:value-of select="../Section/@Code"/>
            <xsl:value-of select="../Class/@Code"/>
            <xsl:value-of select="../Subclass/@Code"/>
            <xsl:value-of select="../Group/@Code"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="../Subgroup/@Code"/>
        </xsl:element>
    </xsl:template>

    <!-- Classification IPC or IPC8 -->
    <xsl:template match="Classification-IPC | Classification-IPC8">
        <xsl:element name="{name(.)}">
            <xsl:attribute name="SequenceNumber">
                <xsl:value-of select="@SequenceNumber"/>
            </xsl:attribute>
            <xsl:element name="Code">
                <!-- section only -->
                <xsl:value-of select="normalize-space(Section)"/>
                <xsl:text> </xsl:text>

                <!-- section + class -->
                <xsl:value-of select="normalize-space(Section)"/>
                <xsl:value-of select="normalize-space(Class)"/>
                <xsl:text> </xsl:text>

                <!-- section + class + subclass -->
                <xsl:value-of select="normalize-space(Section)"/>
                <xsl:value-of select="normalize-space(Class)"/>
                <xsl:value-of select="normalize-space(Subclass)"/>
                <xsl:text> </xsl:text>

                <!-- section + class + subclass + group -->
                <xsl:value-of select="normalize-space(Section)"/>
                <xsl:value-of select="normalize-space(Class)"/>
                <xsl:value-of select="normalize-space(Subclass)"/>
                <xsl:value-of select="normalize-space(Group)"/>
                <xsl:text> </xsl:text>

                <!-- full -->
                <xsl:value-of select="normalize-space(Section)"/>
                <xsl:value-of select="normalize-space(Class)"/>
                <xsl:value-of select="normalize-space(Subclass)"/>
                <xsl:value-of select="normalize-space(Group)"/>
                <xsl:text>/</xsl:text>
                <xsl:value-of select="normalize-space(Subgroup)"/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <!-- Priority Numbers should also include authority -->
    <xsl:template match="Priorities/Priority/Number">
        <xsl:element name="{name(.)}">
            <xsl:value-of select="."/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="../Authority/@Code"/>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <!-- Application Numbers should also include authority -->
    <xsl:template match="Application/Number">
        <xsl:element name="{name(.)}">
            <xsl:value-of select="."/>
            <xsl:text> </xsl:text>
            <xsl:value-of select="../Authority/@Code"/>
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template name="formatDate">
        <xsl:param name="dateStr"/>
        <!-- input format yyyyMMdd -->
        <!-- output format yyyy-MM-dd -->

        <xsl:variable name="yyyy">
            <xsl:value-of select="substring($dateStr,1,4)"/>
        </xsl:variable>

        <xsl:variable name="mm">
            <xsl:value-of select="substring($dateStr,5,2)"/>
        </xsl:variable>

        <xsl:variable name="dd">
            <xsl:value-of select="substring($dateStr,7,2)"/>
        </xsl:variable>

        <xsl:value-of select="$yyyy"/>

        <xsl:if test="not($mm = '00')">
            <xsl:value-of select="'-'"/>
            <xsl:value-of select="$mm"/>
        </xsl:if>
        <xsl:if test="not($dd = '00')">
            <xsl:value-of select="'-'"/>
            <xsl:value-of select="$dd"/>
        </xsl:if>

    </xsl:template>

    <!-- Strip Equivalent Data -->
    <!--  Just match it, and do nothing-->

    <!-- Slow, checks every element (4ms). Can we limit it to match only selected elements or parent? Priority|ClippedImages...
    <xsl:template match="Priority[@Equivalent='true' and @Generated='true'] | ClippedImages[@Equivalent='true' and @Generated='true']" priority="4"/>
    -->
    <xsl:template
            match="Abstract[@Equivalent='true' and @Generated='true'] | Claims[@Equivalent='true'] | Description[@Equivalent='true'] | Title[@Equivalent='true'] | ClippedImages[@Equivalent='true']"
            priority="4"/>
    <!--
      <xsl:template match="//*[@Equivalent='true' and @Generated='true']" priority="4"/>
      -->

    <!-- stripElements -->
    <!--  Just match it, and do nothing-->
    <xsl:template
            match="Envelope|maths|patcit|chemistry|TPGSpecific|AdditionalData|EPOSimpleFamily|heading|Subdivision|Inventor"
            priority="5"/>

    <!--
    <xsl:template match="OrangeBookApplications/OrangeBookApplication/Exclusivities/Exclusivity/text()  | USPairData/TransactionHistory/History/text() | Classifications/node()/*[not(*)]/text()">
    -->

    <!-- Geneal template for fields which can contain line wraps -->
    <!-- Oneliner for better performance -->
    <xsl:template match="*/text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>

</xsl:stylesheet>


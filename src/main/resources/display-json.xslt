﻿<?xml version="1.0" encoding="utf-8"?>

<!-- ╔══════════════════════════════════════════════════════════════════════════════════════════════════╗ -->
<!-- ║ Stylesheet for converting XmlStore Xml to JSON for Summit (Search+Display).                      ║ -->
<!-- ╠══════════════════════════════════════════════════════════════════════════════════════════════════╣ -->
<!-- ║ CHANGE HISTORY:                                                                                  ║ -->
<!-- ╟──────────────────────────────────────────────────────────────────────────────────────────────────╢ -->
<!-- ║  2015/07/21 - v1.0.0                                                                             ║ -->
<!-- ║     Initial version.                                                                             ║ -->
<!-- ║  2015/09/01 - v1.0.1                                                                             ║ -->
<!-- ║     Fulltext Description and Claims HTML-ized.                                                   ║ -->
<!-- ║     Reserved JSON characters escaped (double quotes and backslashes).                            ║ -->
<!-- ║  2015/10/01 - v1.0.2                                                                             ║ -->
<!-- ║     Add document status (Filed, Granted, Ceased) without looking if a granted document exists.   ║ -->
<!-- ║  2016/03/17 - v1.0.3                                                                             ║ -->
<!-- ║     Create separate sections for Biblio, Description & Claims                                    ║ -->
<!-- ║  2016/??/?? - v1.0.4                                                                             ║ -->
<!-- ║     TODO: Add current assignee(s) field & logic                                                  ║ -->
<!-- ║                                                                                                  ║ -->
<!-- ╚══════════════════════════════════════════════════════════════════════════════════════════════════╝ -->
<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:exsl="http://exslt.org/common"
   exclude-result-prefixes="xsl exsl">



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Stylesheet Settings                                                                    ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:output method="text" indent="no" encoding="utf-8" />
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Stylesheet Settings                                                                      ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Stylesheet Parameters                                                                  ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:param name="CurrentDate" select="'20160101'" />
   <xsl:param name="CurrentTime" select="'000000000'" />
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Stylesheet Parameters                                                                    ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Global Variables                                                                       ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:variable name="default.table.width" select="'100%'"/>
   <xsl:variable name="default.img.scaling" select="100"/>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Global Variables                                                                         ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Identity Template (default template)                                                   ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="@generated" mode="text" />
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Identity Template (default template)                                                     ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Master Templates                                                                       ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="/">
      <xsl:variable name="content">
         <xsl:apply-templates select="univentio-patent-document" mode="bib" />
         <xsl:apply-templates select="univentio-patent-document" mode="desc" />
         <xsl:apply-templates select="univentio-patent-document" mode="clms" />
      </xsl:variable>

      <xsl:for-each select="exsl:node-set($content)/*">
         <xsl:if test="position() &gt; 1">
            <xsl:text xml:space="preserve" disable-output-escaping="yes">~~~~~univentio-patent-document~~~~~</xsl:text>
         </xsl:if>

         <xsl:text xml:space="preserve" disable-output-escaping="yes">{&#13;</xsl:text>
         <xsl:value-of select="." />
         <xsl:text xml:space="preserve" disable-output-escaping="yes">&#13;}</xsl:text>
      </xsl:for-each>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Master Templates                                                                         ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Patent Document Templates                                                              ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="univentio-patent-document" mode="bib">
      <xsl:variable name="content">
         <xsl:call-template name="create-json-date-field">
            <xsl:with-param name="name" select="'date-produced'" />
            <xsl:with-param name="value" select="$CurrentDate" />
         </xsl:call-template>
      
         <xsl:call-template name="create-json-time-field">
            <xsl:with-param name="name" select="'time-produced'" />
            <xsl:with-param name="value" select="$CurrentTime" />
         </xsl:call-template>
            
         <xsl:apply-templates select="@InsertDate" />
         <xsl:apply-templates select="@InsertTime" />
         <xsl:apply-templates select="@ChangeDate" />       
         <xsl:apply-templates select="@ChangeTime" />       
         
         <xsl:apply-templates select="self::*" mode="lang" />
         <xsl:apply-templates select="self::*" mode="status" />
     
         <xsl:apply-templates select="Publication" />

         <xsl:apply-templates select="Families" />
               
         <xsl:apply-templates select="Application" />
         <xsl:apply-templates select="BiblioPCT" />
         <xsl:apply-templates select="BiblioEPO" />
         <xsl:apply-templates select="BiblioDocDB" />
         <xsl:apply-templates select="BiblioRest" />
         <xsl:apply-templates select="ExtendedDates" />
         <xsl:apply-templates select="Priorities" />
         <xsl:apply-templates select="ParentsOfContinuation" />
         <xsl:apply-templates select="RelatedDocuments" />
         <xsl:apply-templates select="Classifications" />
         <xsl:apply-templates select="Classifications-FOS" />
         <xsl:apply-templates select="Citations" />
         <xsl:apply-templates select="Persons" />
         <xsl:apply-templates select="Examiners" />
         <xsl:apply-templates select="DesignatedStates" />

         <xsl:apply-templates select="Botanic" />
         <xsl:apply-templates select="Titles" />
         <xsl:apply-templates select="Abstracts" />

         <xsl:apply-templates select="Drawings" />
            
         <xsl:apply-templates select="OrangeBookApplications" />
         <xsl:apply-templates select="PostIssuanceData" />
         <xsl:apply-templates select="LegalEvents" />
         <xsl:apply-templates select="NationalNotifications" />
         <xsl:apply-templates select="USPairData" />
         <xsl:apply-templates select="PrimeNotifiedEvents" />

         <xsl:apply-templates select="Images" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'univentio-patent-document'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   
   <xsl:template match="univentio-patent-document" mode="desc">
      <xsl:variable name="content">
         <xsl:apply-templates select="Publication" />

         <xsl:apply-templates select="Descriptions" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'univentio-patent-document'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   
   <xsl:template match="univentio-patent-document" mode="clms">
      <xsl:variable name="content">
         <xsl:apply-templates select="Publication" />

         <xsl:apply-templates select="Claims" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'univentio-patent-document'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="univentio-patent-document" mode="lang">
      <xsl:variable name="languages">
         <xsl:variable name="array">
            <xsl:apply-templates select="Publication/PublicationLanguage" mode="lang" />
            <xsl:apply-templates select="Titles/Title" mode="lang" />
            <xsl:apply-templates select="Abstracts/Abstract" mode="lang" />
            <xsl:apply-templates select="Descriptions/Description" mode="lang" />
            <xsl:apply-templates select="Claims/Claims" mode="lang" />
         </xsl:variable>

         <!-- Deduplicate languages -->
         <xsl:for-each select="exsl:node-set($array)/*">
            <xsl:variable name="lang" select="text()" />
            <xsl:if test="not(preceding-sibling::*[text() = $lang])">
               <xsl:copy-of select="self::*" />
            </xsl:if>
         </xsl:for-each>
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'languages'" />
         <xsl:with-param name="nodes" select="exsl:node-set($languages)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="*" mode="lang">
      <xsl:variable name="content">
         <xsl:call-template name="create-json-field">
            <xsl:with-param name="name" select="'language'" />
            <xsl:with-param name="value" select="normalize-space(@Lang_ISO639-2)" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="univentio-patent-document" mode="status">
      <xsl:variable name="phases">
         <!--<xsl:variable name="pub-epodoc" select="BiblioDocDB/DocumentNumberEPODOC" />
         <xsl:variable name="pub-docdb" select="concat(Publication/Authority/@Code, BiblioDocDB/DocumentNumberDOCDB)" />
         <xsl:variable name="app-epodoc" select="BiblioDocDB/ApplicationNumberEPODOC" />
         <xsl:variable name="app-docdb" select="concat(Application/Authority/@Code, BiblioDocDB/ApplicationNumberDOCDB)" />-->

         <xsl:variable name="temp">
            <xsl:apply-templates select="PrimeNotifiedEvents" mode="status" />
            <xsl:apply-templates select="Publication" mode="status" />
            <!--<xsl:apply-templates select="EPOSimpleFamily" mode="status">
               <xsl:with-param name="pub-epodoc" select="$pub-epodoc" />
               <xsl:with-param name="pub-docdb" select="$pub-docdb" />
               <xsl:with-param name="app-epodoc" select="$app-epodoc" />
               <xsl:with-param name="app-docdb" select="$app-docdb" />
            </xsl:apply-templates>-->
         </xsl:variable>

         <xsl:for-each select="exsl:node-set($temp)/*">
            <!-- Sort by Date and Event -->
            <!-- (Ceased and Reinstated events may occur on the same date -->
            <!-- and by sorting on Event, Ceased is put before Reinstated) -->
            <xsl:sort select="@Date" data-type="text" order="descending" />
            <xsl:sort select="@Name" data-type="text" order="descending" />

            <xsl:copy-of select="self::*" />
         </xsl:for-each>
      </xsl:variable>

      <xsl:variable name="status">
         <xsl:for-each select="exsl:node-set($phases)/*">
            <xsl:choose>
               <xsl:when test="@Indicator = '?'">
                  <!-- Get previous document state -->
                  <xsl:variable name="status" select="following-sibling::*[@Indicator = '+'][1]/@Name" />
                  <xsl:if test="$status">
                     <Status Name="{$status}" Date="{Date}" />
                  </xsl:if>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:copy-of select="self::*" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:for-each>
      </xsl:variable>

      <xsl:for-each select="exsl:node-set($status)/*">
         <xsl:if test="position() = 1">
            <xsl:text xml:space="preserve" disable-output-escaping="yes">,</xsl:text>
                  
            <xsl:call-template name="create-json-field">
               <xsl:with-param name="name" select="'status'" />
               <xsl:with-param name="value" select="@Name" />
            </xsl:call-template>
         </xsl:if>
      </xsl:for-each>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Patent Document Templates                                                                ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Publication Templates                                                                  ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Publication" mode="status">
      <xsl:choose>
         <xsl:when test="@Status">
            <Status Name="{@Status}" Date="{@StatusDate}" Indicator="+" />
         </xsl:when>
         <xsl:when test="Kind/@Group = 'Application'">
            <Status Name="Filed" Date="{Date}" Indicator="+" />
         </xsl:when>
         <xsl:when test="Kind/@Group = 'Grant'">
            <Status Name="Granted" Date="{Date}" Indicator="+" />
         </xsl:when>
      </xsl:choose>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Publication">
      <xsl:variable name="content">
         <xsl:apply-templates select="@PublicationId" />
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />

         <xsl:apply-templates select="PublicationLanguage" />
         <xsl:apply-templates select="FilingLanguage" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'publication'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Publication/@PublicationId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Publication/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Publication/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Publication/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Publication/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Publication/PublicationLanguage">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'publication-language'" />
         <xsl:with-param name="value" select="normalize-space(@Lang_ISO639-2)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Publication/FilingLanguage">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'filing-language'" />
         <xsl:with-param name="value" select="normalize-space(@Lang_ISO639-2)" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Publication Templates                                                                    ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Family Templates                                                                       ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Families">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="DomesticFamily" />
         <xsl:apply-templates select="MainFamily" />
         <xsl:apply-templates select="CompleteFamily" />
         <xsl:apply-templates select="SimpleFamily" />
         <xsl:apply-templates select="FullFamily" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'families'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Families/DomesticFamily">
      <xsl:variable name="content">
         <xsl:apply-templates select="FamilyId" />
         <xsl:apply-templates select="EarliestDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="FamilyMember" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'member'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'domestic'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Families/DomesticFamily/FamilyId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Families/MainFamily">
      <xsl:variable name="content">
         <xsl:apply-templates select="FamilyId" />
         <xsl:apply-templates select="EarliestDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="FamilyMember" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'member'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'main'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Families/MainFamily/FamilyId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Families/CompleteFamily">
      <xsl:variable name="content">
         <xsl:apply-templates select="FamilyId" />
         <xsl:apply-templates select="EarliestDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="FamilyMember" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'member'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'extended'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="CompleteFamily/FamilyId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Families/SimpleFamily">
      <xsl:variable name="content">
         <xsl:apply-templates select="FamilyId" />

         <xsl:variable name="array">
            <xsl:apply-templates select="FamilyMember" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'member'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'simple'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="SimpleFamily/FamilyId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Families/FullFamily">
      <xsl:variable name="content">
         <xsl:apply-templates select="FamilyId" />
         <xsl:apply-templates select="EarliestDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="FamilyMember" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'member'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'full'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FullFamily/FamilyId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Families/*/EarliestDate">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'earliest-filing-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Families/*/FamilyMember">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="ApplicationDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FamilyMember/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FamilyMember/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FamilyMember/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FamilyMember/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FamilyMember/ApplicationDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'application-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="EPOSimpleFamily" mode="status">
      <xsl:param name="pub-epodoc" />
      <xsl:param name="pub-docdb" />
      <xsl:param name="app-epodoc" />
      <xsl:param name="app-docdb" />
      <xsl:param name="pub-date" />
      <xsl:param name="pub-type" />

      <xsl:variable name="members">
         <xsl:apply-templates select="FamilyMember" mode="status" />
      </xsl:variable>
      <xsl:variable name="related">
         <xsl:variable name="temp" select="exsl:node-set($members)/*[@pub-docdb != $pub-docdb and @app-docdb = $app-docdb]" />
         <xsl:copy-of select="$temp" />
         <xsl:if test="not($temp)">
            <xsl:copy-of select="exsl:node-set($members)/*[@pub-epodoc != $pub-epodoc and @app-epodoc = $app-epodoc]" />
         </xsl:if>
      </xsl:variable>
      <xsl:if test="$related">
         <xsl:choose>
            <xsl:when test="$related/@type = 'Application'">
               <Status Name="Filed" Date="{$pub-date}" Indicator="+" />
            </xsl:when>
            <xsl:when test="$related/@type = 'Grant'">
               <Status Name="Granted" Date="{$pub-date}" Indicator="+" />
            </xsl:when>
            <xsl:when test="$pub-type = 'Grant'">
               <Status Name="Filed" Date="{$pub-date}" Indicator="+" />
            </xsl:when>
            <xsl:otherwise>
               <Status Name="Granted" Date="{$pub-date}" Indicator="+" />
            </xsl:otherwise>
         </xsl:choose>
      </xsl:if>
   </xsl:template>
   <xsl:template match="EPOSimpleFamily/FamilyMember" mode="status">
      <member>
         <xsl:apply-templates select="Publication" mode="status" />
         <xsl:apply-templates select="Application" mode="status" />
      </member>
   </xsl:template>
   <xsl:template match="EPOSimpleFamily/FamilyMember/Publication" mode="status">
      <xsl:attribute name="pub">
         <xsl:value-of select="concat(Authority/@Code, Number)" />
      </xsl:attribute>
      <xsl:attribute name="type">
         <xsl:value-of select="Kind/@Group" />
      </xsl:attribute>
      
      <xsl:apply-templates select="PublicationNumberEPODOC" mode="status" />
   </xsl:template>
   <xsl:template match="EPOSimpleFamily/FamilyMember/Publication/PublicationNumberEPODOC" mode="status">
      <xsl:attribute name="pub-epodoc">
         <xsl:value-of select="ApplicationNumberEPODOC" />
      </xsl:attribute>
   </xsl:template>
   <xsl:template match="EPOSimpleFamily/FamilyMember/Application" mode="status">
      <xsl:attribute name="app">
         <xsl:value-of select="concat(Authority/@Code, Number)" />
      </xsl:attribute>
      
      <xsl:apply-templates select="ApplicationNumberEPODOC" mode="status" />
   </xsl:template>
   <xsl:template match="EPOSimpleFamily/FamilyMember/Application/ApplicationNumberEPODOC" mode="status">
      <xsl:attribute name="app-epodoc">
         <xsl:value-of select="ApplicationNumberEPODOC" />
      </xsl:attribute>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Family Templates                                                                         ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Application Templates                                                                  ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Application">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="SeriesCode" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'application'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Application/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Application/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Application/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Application/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Application/SeriesCode">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'series-code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Application Templates                                                                    ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: PCT Information Templates                                                              ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="BiblioPCT">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="Publication" />
         <xsl:apply-templates select="Application" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'biblio-pct'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="BiblioPCT/Application" priority="1">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="PCT371c124Date" />
         <xsl:apply-templates select="PCTFilingLanguage" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'application'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioPCT/Application/PCT371c124Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'pct-371-c-124-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioPCT/Application/PCTFilingLanguage">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'pct-filing-language'" />
         <xsl:with-param name="value" select="normalize-space(@Lang_ISO639-2)" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="BiblioPCT/Publication" priority="1">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="PCTTranslationDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'publication'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioPCT/Publication/PCTTranslationDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'translation-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: PCT Information Templates                                                                ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: EPO Information Templates                                                              ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="BiblioEPO">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="Application" />
         <xsl:apply-templates select="Publication" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'biblio-epo'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="BiblioEPO/Application" priority="1">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'application'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="BiblioEPO/Publication" priority="1">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="GrantedDate" />
         <xsl:apply-templates select="EPOTranslationDate" />
         <xsl:apply-templates select="ThirdPublicationDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'publication'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioEPO/Publication/GrantedDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'granted-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioEPO/Publication/EPOTranslationDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'translation-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioEPO/Publication/ThirdPublicationDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'third-publication-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: EPO Information Templates                                                                ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: DocDB Information Templates                                                            ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="BiblioDocDB">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="DocdbStatus" />
         <xsl:apply-templates select="self::*" mode="app" />
         <xsl:apply-templates select="self::*" mode="pub" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'biblio-docdb'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="BiblioDocDB/DocdbStatus">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="BiblioDocDB" mode="app">
      <xsl:variable name="content">
         <xsl:apply-templates select="DocdbDocId" />
         <xsl:apply-templates select="ApplicationNumberDOCDB" />
         <xsl:apply-templates select="ApplicationNumberOriginal" />
         <xsl:apply-templates select="ApplicationNumberEPODOC" />
         <xsl:apply-templates select="ApplicationIsRepresentative" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'application'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioDocDB/DocdbDocId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioDocDB/ApplicationNumberDOCDB">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number-docdb'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioDocDB/ApplicationNumberOriginal">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number-original'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioDocDB/ApplicationNumberEPODOC">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number-epodoc'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioDocDB/ApplicationIsRepresentative">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'representative'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="BiblioDocDB" mode="pub">
      <xsl:variable name="content">
         <xsl:apply-templates select="DocdbPublicationId" />
         <xsl:apply-templates select="DocumentNumberDOCDB" />
         <xsl:apply-templates select="DocumentNumberOriginal" />
         <xsl:apply-templates select="DocumentNumberEPODOC" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'publication'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioDocDB/DocdbPublicationId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioDocDB/DocumentNumberDOCDB">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number-docdb'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioDocDB/DocumentNumberOriginal">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number-original'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioDocDB/DocumentNumberEPODOC">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number-epodoc'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: DocDB Information Templates                                                              ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Remaining Biblio Templates                                                             ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="BiblioRest">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="PublicationFilingType" />
         <xsl:apply-templates select="EBDStatus" />
         <xsl:apply-templates select="OriginalPublicationKind" />
         <xsl:apply-templates select="PublicationKindExtended" />
         <xsl:apply-templates select="PreviouslyFiledApplication" />
         <xsl:apply-templates select="KindOfOfficialGazette" />
         <xsl:apply-templates select="AnnualSerialNumber" />
         <xsl:apply-templates select="TermOfGrant" />
         <xsl:apply-templates select="LicenseForSale" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'biblio-rest'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="BiblioRest/PublicationFilingType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'publication-filing-type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/EBDStatus">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'ebd-status'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/OriginalPublicationKind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'original-publication-kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'original-publication-kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/PublicationKindExtended">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'publication-kind-extended'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/PreviouslyFiledApplication">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'previously-filed-application'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/KindOfOfficialGazette">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-of-official-gazette'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/AnnualSerialNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'annual-serial-number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/TermOfGrant">
      <xsl:variable name="content">
         <xsl:apply-templates select="LengthOfGrant" />
         <xsl:apply-templates select="TermExtension" />
         <xsl:apply-templates select="Disclaimer" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'term-of-grant'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/TermOfGrant/LengthOfGrant">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'length-of-grant'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/TermOfGrant/TermExtension">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'term-extension'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/TermOfGrant/Disclaimer">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'disclaimer'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="BiblioRest/LicenseForSale">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'license-for-sale'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Remaining Biblio Templates                                                               ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Common Templates                                                                       ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="ExtendedDates">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="ExtendedDate" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'extended-date'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'extended-dates'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="ExtendedDates/ExtendedDate">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Type" />
         <xsl:apply-templates select="@Bulletin" />
         <xsl:apply-templates select="text()" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ExtendedDate/text()">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ExtendedDate/@Type">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ExtendedDate/@Bulletin">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'bulletin'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Common Templates                                                                         ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Priority Templates                                                                     ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Priorities">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array-1">
            <xsl:apply-templates select="Priority" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'priority'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-1)/*" />
         </xsl:call-template>

         <xsl:variable name="array-2">
            <xsl:apply-templates select="PriorityDOCDB" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'priority-docdb'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-2)/*" />
         </xsl:call-template>

         <xsl:variable name="array-3">
            <xsl:apply-templates select="PriorityOriginal" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'priority-original'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-3)/*" />
         </xsl:call-template>

         <xsl:variable name="array-4">
            <xsl:apply-templates select="PriorityEPODOC" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'priority-epodoc'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-4)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'priorities'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Priority">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Equivalent" />

         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Type" />

         <xsl:variable name="array">
            <xsl:apply-templates select="NationalExtension" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'extension'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Priority/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Priority/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Priority/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Priority/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Priority/Type">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Priority/NationalExtension">
      <xsl:variable name="content">
         <xsl:call-template name="create-json-field">
            <xsl:with-param name="name" select="'issuing-office'" />
            <xsl:with-param name="value" select="normalize-space(@IssuingOffice)" />
         </xsl:call-template>

         <xsl:variable name="value">
            <xsl:apply-templates mode="text" />
         </xsl:variable>
         <xsl:call-template name="create-json-field">
            <xsl:with-param name="name" select="'#text'" />
            <xsl:with-param name="value">
               <xsl:call-template name="escapeJSON">
                  <xsl:with-param name="text" select="$value" />
               </xsl:call-template>         
            </xsl:with-param>
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="PriorityDOCDB">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="LinkageType" />
         <xsl:apply-templates select="ActiveIndicator" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorityDOCDB/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorityDOCDB/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorityDOCDB/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorityDOCDB/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorityDOCDB/LinkageType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'linkage-type'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorityDOCDB/ActiveIndicator">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'active-indicator'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="PriorityOriginal">
      <xsl:variable name="content">
         <xsl:apply-templates select="PublicationNumber" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorityOriginal/PublicationNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="PriorityEPODOC">
      <xsl:variable name="content">
         <xsl:apply-templates select="PublicationNumber" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorityEPODOC/PublicationNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Priority Templates                                                                       ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Parents of Continuation Templates                                                      ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="ParentsOfContinuation">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="ParentOfContinuation" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'parent-of-continuation'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'parents-of-continuation'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ParentsOfContinuation/ParentOfContinuation">
      <xsl:variable name="content">
         <xsl:apply-templates select="Application" />
         <xsl:apply-templates select="Publication" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="ParentOfContinuation/Application" priority="1">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'application'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="ParentOfContinuation/Publication" priority="1">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'publication'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Parents of Continuation Templates                                                        ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Related Documents Templates                                                            ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="RelatedDocuments">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array-1">
            <xsl:apply-templates select="RelatedDocument" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'related-document'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-1)/*" />
         </xsl:call-template>

         <xsl:variable name="array-2">
            <xsl:apply-templates select="ProvisionalApplication" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'provisional-application'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-2)/*" />
         </xsl:call-template>

         <xsl:variable name="array-3">
            <xsl:apply-templates select="PriorPublication" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'prior-publication'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-3)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'related-documents'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="RelatedDocuments/RelatedDocument">
      <xsl:variable name="content">
         <xsl:apply-templates select="@RelationKind" />
         <!--<xsl:apply-templates select="@RelationKindID" />-->

         <xsl:apply-templates select="Child" />
         <xsl:apply-templates select="Parent" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/@RelationKind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'relation-kind'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="RelatedDocument/Child">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'child'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Child/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Child/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Child/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Child/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="RelatedDocument/Parent">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />

         <xsl:apply-templates select="Patent" />
         <xsl:apply-templates select="PCT" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'parent'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="RelatedDocument/Parent/Patent">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'patent'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/Patent/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/Patent/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/Patent/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/Patent/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="RelatedDocument/Parent/PCT">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />

         <xsl:apply-templates select="Date" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'pct'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/PCT/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/PCT/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/PCT/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="RelatedDocument/Parent/PCT/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="RelatedDocuments/ProvisionalApplication">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Status" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ProvisionalApplication/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ProvisionalApplication/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ProvisionalApplication/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ProvisionalApplication/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ProvisionalApplication/Status">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="RelatedDocuments/PriorPublication">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorPublication/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorPublication/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorPublication/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PriorPublication/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Related Documents Templates                                                              ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Classifications Templates                                                              ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Classifications">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array-1">
            <xsl:apply-templates select="Classification-IPC" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'ipc'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-1)/*" />
         </xsl:call-template>

         <xsl:variable name="array-2">
            <xsl:apply-templates select="Classification-IPC8" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'ipc8'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-2)/*" />
         </xsl:call-template>

         <xsl:variable name="array-3">
            <xsl:apply-templates select="Classification-ECLA" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'ecla'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-3)/*" />
         </xsl:call-template>

         <xsl:variable name="array-4">
            <xsl:apply-templates select="Classification-ICO" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'ico'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-4)/*" />
         </xsl:call-template>

         <xsl:variable name="array-5">
            <xsl:apply-templates select="Classification-IDT" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'idt'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-5)/*" />
         </xsl:call-template>

         <xsl:variable name="array-6">
            <xsl:apply-templates select="Classification-ECNO" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'ecno'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-6)/*" />
         </xsl:call-template>

         <xsl:variable name="array-7">
            <xsl:apply-templates select="Classification-National" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'national'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-7)/*" />
         </xsl:call-template>

         <xsl:variable name="array-8">
            <xsl:apply-templates select="Classification-US" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'us'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-8)/*" />
         </xsl:call-template>

         <xsl:variable name="array-9">
            <xsl:apply-templates select="Classification-JPFI" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'jp-fi'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-9)/*" />
         </xsl:call-template>

         <xsl:variable name="array-10">
            <xsl:apply-templates select="Classification-JPFTERM" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'jp-f-term'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-10)/*" />
         </xsl:call-template>

         <xsl:variable name="array-11">
            <xsl:apply-templates select="Classification-Locarno" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'locarno'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-11)/*" />
         </xsl:call-template>

         <xsl:variable name="array-12">
            <xsl:apply-templates select="Classification-CPC" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'cpc'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-12)/*" />
         </xsl:call-template>

         <xsl:variable name="array-13">
            <xsl:apply-templates select="Classification-CPCNO" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'cpcno'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-13)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'classifications'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-IPC">
      <xsl:variable name="content">
         <xsl:apply-templates select="Type" />
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Edition" />
         <xsl:apply-templates select="Section" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
         <xsl:apply-templates select="Group" />
         <xsl:apply-templates select="Subgroup" />
         <xsl:apply-templates select="QChar" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC/Type">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC/Edition">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'edition'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC/Section">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'section'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC/Group">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'group'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC/Subgroup">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subgroup'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC/QChar">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'qualifying-char'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-IPC8">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Section" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
         <xsl:apply-templates select="Group" />
         <xsl:apply-templates select="Subgroup" />
         <xsl:apply-templates select="EditionDate" />
         <xsl:apply-templates select="ClassificationLevel" />
         <xsl:apply-templates select="SymbolPosition" />
         <xsl:apply-templates select="ClassificationValue" />
         <xsl:apply-templates select="ActionDate" />
         <xsl:apply-templates select="ClassificationStatus" />
         <xsl:apply-templates select="DataSource" />
         <xsl:apply-templates select="Authority" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/Section">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'section'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/Group">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'group'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/Subgroup">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subgroup'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/EditionDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'edition-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/ClassificationLevel">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'level'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/SymbolPosition">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'position'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/ClassificationValue">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'value'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/ActionDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'action-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/ClassificationStatus">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/DataSource">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'data-source'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IPC8/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-ECLA">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Section" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
         <xsl:apply-templates select="Group" />
         <xsl:apply-templates select="Subgroup" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ECLA/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ECLA/Section">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'section'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ECLA/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ECLA/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ECLA/Group">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'group'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ECLA/Subgroup">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subgroup'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-ICO">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Section" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
         <xsl:apply-templates select="Group" />
         <xsl:apply-templates select="Subgroup" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ICO/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ICO/Section">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'section'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ICO/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ICO/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ICO/Group">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'group'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ICO/Subgroup">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subgroup'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-IDT">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-IDT/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-ECNO">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Code" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ECNO/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-ECNO/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-National">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-National/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-National/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-National/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-National/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-US">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-US/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-US/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-US/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-JPFI">
      <xsl:variable name="content">
         <xsl:apply-templates select="Type" />
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Section" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
         <xsl:apply-templates select="Group" />
         <xsl:apply-templates select="Subgroup" />
         <xsl:apply-templates select="FICode" />
         <xsl:apply-templates select="Facet" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFI/Type">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFI/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFI/Section">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'section'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFI/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFI/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFI/Group">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'group'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFI/Subgroup">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subgroup'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFI/FICode">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'fi-code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFI/Facet">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'facet'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-JPFTERM">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Theme" />
         <xsl:apply-templates select="Viewpoint" />
         <xsl:apply-templates select="Figure" />
         <xsl:apply-templates select="Additional" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFTERM/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFTERM/Theme">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'theme'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFTERM/Viewpoint">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'viewpoint'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFTERM/Figure">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'figure'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-JPFTERM/Additional">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'additional'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-Locarno">
      <xsl:variable name="content">
         <xsl:apply-templates select="Edition" />
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-Locarno/Edition">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'edition'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-Locarno/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-Locarno/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-Locarno/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-CPC">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Section" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
         <xsl:apply-templates select="Group" />
         <xsl:apply-templates select="Subgroup" />
         <xsl:apply-templates select="EditionDate" />
         <xsl:apply-templates select="SymbolPosition" />
         <xsl:apply-templates select="ClassificationValue" />
         <xsl:apply-templates select="ActionDate" />
         <xsl:apply-templates select="ClassificationStatus" />
         <xsl:apply-templates select="DataSource" />
         <xsl:apply-templates select="Authority" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/Section">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'section'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/Group">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'group'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/Subgroup">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subgroup'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/EditionDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'edition-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/SymbolPosition">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'position'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/ClassificationValue">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'value'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/ActionDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'action-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/ClassificationStatus">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/DataSource">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'data-source'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPC/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications/Classification-CPCNO">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Section" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
         <xsl:apply-templates select="Group" />
         <xsl:apply-templates select="Subgroup" />
         <xsl:apply-templates select="EditionDate" />
         <xsl:apply-templates select="SymbolPosition" />
         <xsl:apply-templates select="ClassificationValue" />
         <xsl:apply-templates select="ActionDate" />
         <xsl:apply-templates select="ClassificationStatus" />
         <xsl:apply-templates select="DataSource" />
         <xsl:apply-templates select="Authority" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/Section">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'section'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/Group">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'group'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/Subgroup">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subgroup'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/EditionDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'edition-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/SymbolPosition">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'position'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/ClassificationValue">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'value'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/ActionDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'action-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/ClassificationStatus">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/DataSource">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'data-source'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classification-CPCNO/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value">
            <xsl:choose>
               <xsl:when test="@Code">
                  <xsl:value-of select="normalize-space(@Code)" />
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="normalize-space()" />
               </xsl:otherwise>
            </xsl:choose>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Classifications Templates                                                                ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Field of Search Templates                                                              ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Classifications-FOS">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array-1">
            <xsl:apply-templates select="Classification-IPC" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'ipc'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-1)/*" />
         </xsl:call-template>

         <xsl:variable name="array-2">
            <xsl:apply-templates select="Classification-IPC8" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'ipc8'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-2)/*" />
         </xsl:call-template>

         <xsl:variable name="array-3">
            <xsl:apply-templates select="Classification-USFOS" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'us'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-3)/*" />
         </xsl:call-template>

         <xsl:variable name="array-4">
            <xsl:apply-templates select="Classification-CPC" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'cpc'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-4)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'field-of-search'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications-FOS/Classification-IPC">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classifications-FOS/Classification-IPC/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications-FOS/Classification-IPC8">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classifications-FOS/Classification-IPC8/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications-FOS/Classification-USFOS">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="Class" />
         <xsl:apply-templates select="Subclass" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classifications-FOS/Classification-USFOS/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classifications-FOS/Classification-USFOS/Class">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'class'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classifications-FOS/Classification-USFOS/Subclass">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subclass'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Classifications-FOS/Classification-CPC">
      <xsl:variable name="content">
         <xsl:apply-templates select="Code" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Classifications-FOS/Classification-CPC/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Field of Search Templates                                                                ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Citations Templates                                                                    ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Citations">
      <xsl:variable name="content">
         <xsl:apply-templates select="PatentCitations" />
         <xsl:apply-templates select="NonPatentCitations" />
         <xsl:apply-templates select="ForwardPatentCitations" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'citations'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Citations/PatentCitations">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="PatentCitation" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'citation'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'patent'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Citations/NonPatentCitations">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="NonPatentCitation" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'citation'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'non-patent'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Citations/ForwardPatentCitations">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="ForwardPatentCitation" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'citation'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'forward'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="PatentCitations/PatentCitation">
      <xsl:variable name="content">
         <xsl:apply-templates select="@PublicationId" />
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="ApplicationDate" />
         <xsl:apply-templates select="Relevance" />
         <xsl:apply-templates select="Origin" />
         <xsl:apply-templates select="SearchOffice" />
         <xsl:apply-templates select="Inventor" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentCitation/@PublicationId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentCitation/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentCitation/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentCitation/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentCitation/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentCitation/ApplicationDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'application-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentCitation/Relevance">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'relevance'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentCitation/Origin">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'origin'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'phase'" />
         <xsl:with-param name="value" select="normalize-space(@Phase)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentCitation/SearchOffice">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'search-office'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentCitation/Inventor">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'inventor'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="NonPatentCitations/NonPatentCitation">
      <xsl:variable name="content">
         <xsl:apply-templates select="Citation" />
         <xsl:apply-templates select="DocumentNumber" />
         <xsl:apply-templates select="Relevance" />
         <xsl:apply-templates select="Origin" />
         <xsl:apply-templates select="SearchOffice" />
         <xsl:apply-templates select="ScopusId" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NonPatentCitation/Citation">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'text'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NonPatentCitation/DocumentNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'document-number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NonPatentCitation/Relevance">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'relevance'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NonPatentCitation/Origin">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'origin'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'phase'" />
         <xsl:with-param name="value" select="normalize-space(@Phase)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NonPatentCitation/SearchOffice">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'search-office'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NonPatentCitation/ScopusId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'scopus-id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'scopus-count'" />
         <xsl:with-param name="value" select="normalize-space(@CitedByCount)" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="ForwardPatentCitations/ForwardPatentCitation">
      <xsl:variable name="content">
         <xsl:apply-templates select="@PublicationId" />
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Kind" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="ApplicationDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ForwardPatentCitation/@PublicationId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ForwardPatentCitation/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ForwardPatentCitation/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ForwardPatentCitation/Kind">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'kind-id'" />
         <xsl:with-param name="value" select="normalize-space(@CodeId)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ForwardPatentCitation/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ForwardPatentCitation/ApplicationDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'application-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Citations Templates                                                                      ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Persons Templates                                                                      ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Persons">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array-1">
            <xsl:apply-templates select="Person[@PersonRole = 'applicant' and not(@DataFormat)]" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'applicant'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-1)/*" />
         </xsl:call-template>

         <xsl:variable name="array-2">
            <xsl:apply-templates select="Person[@PersonRole = 'inventor' and not(@DataFormat)]" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'inventor'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-2)/*" />
         </xsl:call-template>

         <xsl:variable name="array-3">
            <xsl:apply-templates select="Person[@PersonRole = 'attorney' and not(@DataFormat)]" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'attorney'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-3)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'persons'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Persons/Person[@PersonRole = 'applicant']">
      <xsl:variable name="content">
         <xsl:apply-templates select="@PersonRole" />
         <!--<xsl:apply-templates select="@PersonRoleId" />-->
         <!--<xsl:apply-templates select="@DataFormat" />-->
         <xsl:apply-templates select="@PersonType" />
         <xsl:apply-templates select="@Equivalent" />
         <!--<xsl:apply-templates select="@Type" />-->
         <xsl:apply-templates select="AddressbookLanguage" />
         <xsl:apply-templates select="Name" />
         <xsl:if test="not(Name)">
            <xsl:apply-templates select="SurName" mode="concat" />
            <xsl:if test="not(SurName)">
               <xsl:apply-templates select="FirstName" mode="concat" />
            </xsl:if>
         </xsl:if>
         <xsl:apply-templates select="FirstName" />
         <xsl:apply-templates select="SurName" />
         <xsl:apply-templates select="Address" />
         <xsl:apply-templates select="PostCode" />
         <xsl:apply-templates select="City" />
         <xsl:apply-templates select="Subdivision" />
         <xsl:apply-templates select="Country" />
         <xsl:apply-templates select="Nationality" />
         <xsl:apply-templates select="Residence" />
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="RegisteredNumber" />
         <xsl:apply-templates select="IssuingOffice" />
         <xsl:apply-templates select="Extended/Name" />
         <xsl:apply-templates select="Extended/Type" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Persons/Person[@PersonRole = 'inventor']">
      <xsl:variable name="content">
         <xsl:apply-templates select="@PersonRole" />
         <!--<xsl:apply-templates select="@PersonRoleID" />-->
         <!--<xsl:apply-templates select="@DataFormat" />-->
         <xsl:apply-templates select="@PersonType" />
         <xsl:apply-templates select="@Equivalent" />
         <!--<xsl:apply-templates select="@Type" />-->
         <xsl:apply-templates select="AddressbookLanguage" />
         <xsl:apply-templates select="Name" />
         <xsl:if test="not(Name)">
            <xsl:apply-templates select="SurName" mode="concat" />
            <xsl:if test="not(SurName)">
               <xsl:apply-templates select="FirstName" mode="concat" />
            </xsl:if>
         </xsl:if>
         <xsl:apply-templates select="FirstName" />
         <xsl:apply-templates select="SurName" />
         <xsl:apply-templates select="Address" />
         <xsl:apply-templates select="PostCode" />
         <xsl:apply-templates select="City" />
         <xsl:apply-templates select="Subdivision" />
         <xsl:apply-templates select="Country" />
         <xsl:apply-templates select="Nationality" />
         <xsl:apply-templates select="Residence" />
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="RegisteredNumber" />
         <xsl:apply-templates select="IssuingOffice" />
         <xsl:apply-templates select="Extended/Name" />
         <xsl:apply-templates select="Extended/Type" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Persons/Person[@PersonRole = 'attorney']">
      <xsl:variable name="content">
         <xsl:apply-templates select="@PersonRole" />
         <!--<xsl:apply-templates select="@PersonRoleID" />-->
         <!--<xsl:apply-templates select="@DataFormat" />-->
         <xsl:apply-templates select="@PersonType" />
         <xsl:apply-templates select="@Equivalent" />
         <!--<xsl:apply-templates select="@Type" />-->
         <xsl:apply-templates select="AddressbookLanguage" />
         <xsl:apply-templates select="Name" />
         <xsl:if test="not(Name)">
            <xsl:apply-templates select="SurName" mode="concat" />
            <xsl:if test="not(SurName)">
               <xsl:apply-templates select="FirstName" mode="concat" />
            </xsl:if>
         </xsl:if>
         <xsl:apply-templates select="FirstName" />
         <xsl:apply-templates select="SurName" />
         <xsl:apply-templates select="Address" />
         <xsl:apply-templates select="PostCode" />
         <xsl:apply-templates select="City" />
         <xsl:apply-templates select="Subdivision" />
         <xsl:apply-templates select="Country" />
         <xsl:apply-templates select="Nationality" />
         <xsl:apply-templates select="Residence" />
         <xsl:apply-templates select="Code" />
         <xsl:apply-templates select="RegisteredNumber" />
         <xsl:apply-templates select="IssuingOffice" />
         <xsl:apply-templates select="Extended/Name" />
         <xsl:apply-templates select="Extended/Type" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Person/@PersonRole">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'role'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!--<xsl:template match="Person/@PersonRoleID" />-->
   <!--<xsl:template match="Person/@DataFormat" />-->
   <xsl:template match="Person/@PersonType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!--<xsl:template match="Person/@Type" />-->
   <xsl:template match="Person/AddressbookLanguage">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'addressbook-language'" />
         <xsl:with-param name="value" select="normalize-space(@Lang_ISO639-2)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/FirstName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'first-name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/SurName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'last-name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/Address">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/PostCode">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'postcode'" />
         <xsl:with-param name="value" select="$value" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/City">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'city'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/Subdivision">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'state'" />
         <xsl:with-param name="value" select="normalize-space(substring-after(@Code, '-'))" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/Country">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'country'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/Nationality">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'nationality'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/Residence">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'residence'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/RegisteredNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'registered-number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/IssuingOffice">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'issuing-office'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/Extended/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="concat('name-', @Type)" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/Extended/Type">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type-standardized'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/SurName" mode="concat">
      <xsl:variable name="value">
         <xsl:if test="parent::*/FirstName">
            <xsl:apply-templates select="parent::*/FirstName" mode="no-breaks" />
         </xsl:if>
         <xsl:apply-templates select="self::*" mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Person/FirstName" mode="concat">
      <xsl:variable name="value">
         <xsl:apply-templates select="self::*" mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Persons Templates                                                                        ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Examiners Templates                                                                    ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Examiners">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="PrimaryExaminer" />
         <xsl:apply-templates select="AssistantExaminer" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'examiners'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Examiners/PrimaryExaminer">
      <xsl:variable name="content">
         <xsl:apply-templates select="AddressbookLanguage" />
         <xsl:apply-templates select="Name" />
         <xsl:if test="not(Name)">
            <xsl:apply-templates select="SurName" mode="concat" />
            <xsl:if test="not(SurName)">
               <xsl:apply-templates select="FirstName" mode="concat" />
            </xsl:if>
         </xsl:if>
         <xsl:apply-templates select="FirstName" />
         <xsl:apply-templates select="SurName" />
         <xsl:apply-templates select="Department" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'primary'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimaryExaminer/AddressbookLanguage">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'addressbook-language'" />
         <xsl:with-param name="value" select="normalize-space(@Lang_ISO639-2)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimaryExaminer/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimaryExaminer/FirstName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'first-name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimaryExaminer/SurName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'last-name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimaryExaminer/Department">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'department'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimaryExaminer/SurName" mode="concat">
      <xsl:variable name="value">
         <xsl:if test="parent::*/FirstName">
            <xsl:apply-templates select="parent::*/FirstName" mode="no-breaks" />
         </xsl:if>
         <xsl:apply-templates select="self::*" mode="no-breaks" />
      </xsl:variable>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimaryExaminer/FirstName" mode="concat">
      <xsl:variable name="value">
         <xsl:apply-templates select="self::*" mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Examiners/AssistantExaminer">
      <xsl:variable name="content">
         <xsl:apply-templates select="Name" />
         <xsl:if test="not(Name)">
            <xsl:apply-templates select="SurName" mode="concat" />
            <xsl:if test="not(SurName)">
               <xsl:apply-templates select="FirstName" mode="concat" />
            </xsl:if>
         </xsl:if>
         <xsl:apply-templates select="FirstName" />
         <xsl:apply-templates select="SurName" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'assistant'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AssistantExaminer/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AssistantExaminer/FirstName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'first-name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AssistantExaminer/SurName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'last-name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AssistantExaminer/SurName" mode="concat">
   
      <xsl:variable name="value">
         <xsl:if test="parent::*/FirstName">
            <xsl:apply-templates select="parent::*/FirstName" mode="no-breaks" />
         </xsl:if>
         <xsl:apply-templates select="self::*" mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AssistantExaminer/FirstName" mode="concat">
      <xsl:variable name="value">
         <xsl:apply-templates select="self::*" mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Examiners Templates                                                                      ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Designated States Templates                                                            ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="DesignatedStates">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="LapsesOfPatent" />
         <xsl:apply-templates select="DesignatedStates" />
         <xsl:apply-templates select="ExtendedDesignatedStates" />
         <xsl:apply-templates select="ValidationStates" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'designated-states'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="DesignatedStates/LapsesOfPatent">
      <xsl:variable name="array">
         <xsl:apply-templates select="Authority" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'lapse of patent'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LapsesOfPatent/Authority">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Code" />
         <xsl:apply-templates select="@LapseDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LapsesOfPatent/Authority/@Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'state'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LapsesOfPatent/Authority/@LapseDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="DesignatedStates/DesignatedStates[@DesignatedStatesRole = 'regionalContracting']">
      <xsl:variable name="array">
         <xsl:apply-templates select="Authority" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'contracting'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DesignatedStates/DesignatedStates[@DesignatedStatesRole = 'national']">
      <xsl:variable name="array">
         <xsl:apply-templates select="Authority" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'national'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DesignatedStates/DesignatedStates[@DesignatedStatesRole = 'regionalEuropean']">
      <xsl:variable name="array">
         <xsl:apply-templates select="Authority" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'regional-european'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DesignatedStates/DesignatedStates[@DesignatedStatesRole = 'regionalEurAsian']">
      <xsl:variable name="array">
         <xsl:apply-templates select="Authority" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'regional-eurasian'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DesignatedStates/DesignatedStates[@DesignatedStatesRole = 'regionalARIPO']">
      <xsl:variable name="array">
         <xsl:apply-templates select="Authority" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'regional-aripo'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DesignatedStates/DesignatedStates[@DesignatedStatesRole = 'regionalOAPI']">
      <xsl:variable name="array">
         <xsl:apply-templates select="Authority" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'regional-oapi'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DesignatedStates/Authority">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Code" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DesignatedStates/Authority/@Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'state'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="DesignatedStates/ExtendedDesignatedStates">
      <xsl:variable name="array">
         <xsl:apply-templates select="Authority" />
      </xsl:variable>
      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'extended-state'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ExtendedDesignatedStates/Authority">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Code" />
         <xsl:apply-templates select="@PaymentDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ExtendedDesignatedStates/Authority/@Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'state'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ExtendedDesignatedStates/Authority/@PaymentDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="DesignatedStates/ValidationStates">
      <xsl:variable name="array">
         <xsl:apply-templates select="Authority" />
      </xsl:variable>
      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'validation-state'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ValidationStates/Authority">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Code" />
         <xsl:apply-templates select="@PaymentDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ValidationStates/Authority/@Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'state'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ValidationStates/Authority/@PaymentDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Designated States Templates                                                              ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Botanic Templates                                                                      ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Botanic">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="LatinName" />
         <xsl:apply-templates select="Variety" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'botanic'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Botanic/LatinName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'latin-name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Botanic/Variety">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'variety'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Botanic Templates                                                                        ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Titles Templates                                                                       ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Titles">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="Title" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'title'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'titles'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Titles/Title">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Lang_ISO639-2" />
         <xsl:apply-templates select="@DataType" />
         <xsl:apply-templates select="@Equivalent" />
         <!--<xsl:apply-templates select="@DataTypeId" />-->

         <xsl:apply-templates select="self::*" mode="html" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Title/@Lang_ISO639-2">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'language'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Title/@DataType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'data-type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!--<xsl:template match="Titles/Title/@DataTypeId" />-->
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Titles Templates                                                                         ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Abstracts Templates                                                                    ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Abstracts">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="Abstract" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'abstract'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'abstracts'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Abstracts/Abstract">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Lang_ISO639-2" />
         <xsl:apply-templates select="@DataType" />
         <xsl:apply-templates select="@Equivalent" />
         <!--<xsl:apply-templates select="@DataTypeId" />-->

         <xsl:apply-templates select="self::*" mode="html" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Abstract/@Lang_ISO639-2">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'language'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Abstract/@DataType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'data-type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!--<xsl:template match="Abstracts/Abstract/@DataTypeId" />-->
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Abstracts Templates                                                                      ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Descriptions Templates                                                                 ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Descriptions">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="Description" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'description'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'descriptions'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Descriptions/Description">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Lang_ISO639-2" />
         <xsl:apply-templates select="@DataType" />
         <xsl:apply-templates select="@Equivalent" />
         <!--<xsl:apply-templates select="@DataTypeId" />-->

         <xsl:apply-templates select="self::*" mode="html" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Description/@Lang_ISO639-2">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'language'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Description/@DataType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'data-type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!--<xsl:template match="Description/@DataTypeId" />-->
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Descriptions Templates                                                                   ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Claims Templates                                                                       ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Claims">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="Claims" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'claims'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'claims'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Claims/Claims">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Lang_ISO639-2" />
         <xsl:apply-templates select="@DataType" />
         <xsl:apply-templates select="@Equivalent" />
         <xsl:apply-templates select="@ClaimSet" />
         <!--<xsl:apply-templates select="@DataTypeId" />-->

         <xsl:apply-templates select="Claim[@SequenceNumber = 0]" />

         <xsl:variable name="array">
            <xsl:apply-templates select="Claim[@SequenceNumber &gt; 0]" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'claim'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Claims/@Lang_ISO639-2">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'language'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Claims/@DataType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'data-type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Claims/@ClaimSet">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'claimset'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!--<xsl:template match="Claims/@DataTypeId" />-->
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Claims/Claim[@SequenceNumber = 0]" priority="1">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'claim-statement'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space()" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Claims/Claim">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ClaimId" />
         <xsl:apply-templates select="@ClaimNo" />
         <xsl:apply-templates select="@Exemplary" />
         <xsl:apply-templates select="@Independent" />

         <xsl:apply-templates select="self::*" mode="html" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Claim/@ClaimId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Claim/@ClaimNo">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'num'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Claim/@Exemplary">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'exemplary'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Claim/@Independent">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'independent'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Claims Templates                                                                         ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: OrangeBook Applications Templates                                                      ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="OrangeBookApplications">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="OrangeBookApplication" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'application'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'orangebook-applications'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="OrangeBookApplications/OrangeBookApplication">
      <xsl:variable name="content">
         <xsl:apply-templates select="ApplicationType" />
         <xsl:apply-templates select="ApplicationNumber" />
         <xsl:apply-templates select="ProductNumber" />
         <xsl:apply-templates select="ExpiryDate" />
         <xsl:apply-templates select="DrugSubstanceClaim" />
         <xsl:apply-templates select="DrugProductClaim" />
         <xsl:apply-templates select="DelistFlag" />

         <xsl:variable name="array-1">
            <xsl:apply-templates select="PatentUsage/PatentUse" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'patent-usage'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-1)/*" />
         </xsl:call-template>

         <xsl:apply-templates select="Product" />

         <xsl:variable name="array-2">
            <xsl:apply-templates select="Exclusivities/Exclusivity" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'exclusivity'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-2)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="OrangeBookApplication/ApplicationType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'application-type'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/ApplicationNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'application-number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/ProductNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'product-number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/ExpiryDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'expiry-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/DrugSubstanceClaim">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'drug-substance-claim'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/DrugProductClaim">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'drug-product-claim'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/DelistFlag">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'delist-flag'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentUsage/PatentUse">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Code" />
         <xsl:apply-templates select="text()" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentUsage/PatentUse/@Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentUsage/PatentUse/text()">
      <xsl:variable name="value">
         <xsl:apply-templates select="self::text()" mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'#text'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="OrangeBookApplication/Product">
      <xsl:variable name="content">
         <xsl:apply-templates select="Ingredient" />
         <xsl:apply-templates select="DosageForm" />
         <xsl:apply-templates select="Route" />
         <xsl:apply-templates select="ProprietaryName" />
         <xsl:apply-templates select="Applicant" />
         <xsl:apply-templates select="ApplicantFullName" />
         <xsl:apply-templates select="Strength" />

         <xsl:variable name="array">
            <xsl:apply-templates select="TherapeuticEquivalences/TherapeuticEquivalence" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'therapeutic-equivalence'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>

         <xsl:apply-templates select="ApprovalDate" />
         <xsl:apply-templates select="ApprovalDateRemark" />
         <xsl:apply-templates select="ReferenceListedDrug" />
         <xsl:apply-templates select="ProductType" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'product'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/Ingredient">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'ingredient'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/DosageForm">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'dosage-form'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/Route">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'route'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/ProprietaryName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'proprietary-name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/Applicant">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'applicant'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/ApplicantFullName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'applicant-fullname'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/Strength">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'strength'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Product/TherapeuticEquivalences/TherapeuticEquivalence">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Code" />
         <xsl:apply-templates select="text()" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="TherapeuticEquivalences/TherapeuticEquivalence/@Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="TherapeuticEquivalences/TherapeuticEquivalence/text()">
      <xsl:variable name="value">
         <xsl:apply-templates select="self::text()" mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'#text'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/ApprovalDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'approval-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/ApprovalDateRemark">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'approval-date-remark'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/ReferenceListedDrug">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'reference-listed-drug'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Product/ProductType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'product-type'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="OrangeBookApplication/Exclusivities/Exclusivity">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Code" />
         <xsl:apply-templates select="@Date" />
         <xsl:apply-templates select="text()" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Exclusivities/Exclusivity/@Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Exclusivities/Exclusivity/@Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Exclusivities/Exclusivity/text()">
      <xsl:variable name="value">
         <xsl:apply-templates select="self::text()" mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'#text'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: OrangeBook Applications Templates                                                        ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Post-Issuance Data Templates                                                           ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="PostIssuanceData">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array-1">
            <xsl:apply-templates select="Expirations/Expiration" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'expiration'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-1)/*" />
         </xsl:call-template>

         <xsl:variable name="array-2">
            <xsl:apply-templates select="Reinstatements/Reinstatement" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'reinstatement'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-2)/*" />
         </xsl:call-template>

         <xsl:variable name="array-3">
            <xsl:apply-templates select="Reissues/Reissue" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'reissue'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-3)/*" />
         </xsl:call-template>

         <xsl:variable name="array-4">
            <xsl:apply-templates select="Disclaimers/Disclaimer" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'disclaimer'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-4)/*" />
         </xsl:call-template>

         <xsl:variable name="array-5">
            <xsl:apply-templates select="CertificateOfCorrections/CertificateOfCorrection" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'certificate-of-correction'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-5)/*" />
         </xsl:call-template>

         <xsl:variable name="array-6">
            <xsl:apply-templates select="Reexaminations/Reexamination" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'reexamination'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-6)/*" />
         </xsl:call-template>

         <xsl:variable name="array-7">
            <xsl:apply-templates select="NoticesOfLitigation/NoticeOfLitigation" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'notice-of-litigation'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-7)/*" />
         </xsl:call-template>

         <xsl:variable name="array-8">
            <xsl:apply-templates select="AdverseDecisions/AdverseDecision" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'adverse-decision'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-8)/*" />
         </xsl:call-template>

         <xsl:variable name="array-9">
            <xsl:apply-templates select="DailyAssignments/DailyAssignment" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'daily-assignment'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-9)/*" />
         </xsl:call-template>

         <xsl:variable name="array-10">
            <xsl:apply-templates select="ErratumReferences/ErratumReference" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'erratum-reference'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-10)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'post-issuance'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Expirations/Expiration">
      <xsl:variable name="content">
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Comment" />
         <xsl:apply-templates select="OGDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Expiration/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Expiration/Comment">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'comment'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Expiration/OGDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'og-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Reinstatements/Reinstatement">
      <xsl:variable name="content">
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Comment" />
         <xsl:apply-templates select="OGDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Reinstatement/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Reinstatement/Comment">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'comment'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Reinstatement/OGDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'og-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Reissues/Reissue">
      <xsl:variable name="content">
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Comment" />
         <xsl:apply-templates select="OGDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Reissue/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Reissue/Comment">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'comment'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Reissue/OGDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'og-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Disclaimers/Disclaimer">
      <xsl:variable name="content">
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Comment" />
         <xsl:apply-templates select="OGDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Disclaimer/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Disclaimer/Comment">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'comment'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Disclaimer/OGDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'og-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="CertificateOfCorrections/CertificateOfCorrection">
      <xsl:variable name="content">
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Comment" />
         <xsl:apply-templates select="OGDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="CertificateOfCorrection/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="CertificateOfCorrection/Comment">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'comment'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="CertificateOfCorrection/OGDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'og-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Reexaminations/Reexamination">
      <xsl:variable name="content">
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Comment" />
         <xsl:apply-templates select="OGDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Reexamination/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Reexamination/Comment">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'comment'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Reexamination/OGDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'og-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="NoticesOfLitigation/NoticeOfLitigation">
      <xsl:variable name="content">
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Comment" />
         <xsl:apply-templates select="Judgement" />
         <xsl:apply-templates select="DocketNumber" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NoticeOfLitigation/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NoticeOfLitigation/Comment">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'comment'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NoticeOfLitigation/Judgement">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'judgement'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NoticeOfLitigation/DocketNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'docket-number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="AdverseDecisions/AdverseDecision">
      <xsl:variable name="content">
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Comment" />
         <xsl:apply-templates select="OGDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AdverseDecision/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AdverseDecision/Comment">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'comment'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AdverseDecision/OGDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'og-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="DailyAssignments/DailyAssignment">
      <xsl:variable name="content">
         <xsl:apply-templates select="@AssignmentType" />
         <xsl:apply-templates select="ReelFrameNumber" />
         <xsl:apply-templates select="RecordedDate" />
         <xsl:apply-templates select="PurgeIndicator" />
         <xsl:apply-templates select="Comment" />
         <xsl:apply-templates select="Correspondent" />

         <xsl:variable name="array-1">
            <xsl:apply-templates select="Assignees/Assignee" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'assignee'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-1)/*" />
         </xsl:call-template>

         <xsl:variable name="array-2">
            <xsl:apply-templates select="Assignors/Assignor" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'assignor'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-2)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DailyAssignment/@AssignmentType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DailyAssignment/ReelFrameNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'reel-frame'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DailyAssignment/RecordedDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'recorded-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DailyAssignment/PurgeIndicator">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'purge-indicator'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="DailyAssignment/Comment">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'comment'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="DailyAssignment/Correspondent">
      <xsl:variable name="content">
         <xsl:apply-templates select="Name" />
         <xsl:apply-templates select="Address1" mode="concat" />
         <xsl:apply-templates select="Address1" />
         <xsl:apply-templates select="Address2" />
         <xsl:apply-templates select="Address3" />
         <xsl:apply-templates select="Address4" />
         <xsl:apply-templates select="Address5" />
         <xsl:apply-templates select="PostCode" />
         <xsl:apply-templates select="City" />
         <xsl:apply-templates select="US-State" />
         <xsl:apply-templates select="Country" />
         <xsl:apply-templates select="Extended/Name" />
         <xsl:apply-templates select="Extended/Type" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'correspondent'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/Address1" mode="concat">
      <!-- Concatenate all address lines into one single line -->
      <xsl:variable name="value">
         <xsl:apply-templates select="self::*" mode="no-breaks" />
         <xsl:if test="following-sibling::Address2">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address2" mode="no-breaks" />
         </xsl:if>
         <xsl:if test="following-sibling::Address3">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address3" mode="no-breaks" />
         </xsl:if>
         <xsl:if test="following-sibling::Address4">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address4" mode="no-breaks" />
         </xsl:if>
         <xsl:if test="following-sibling::Address5">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address5" mode="no-breaks" />
         </xsl:if>
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/Address1">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-1'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/Address2">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-2'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/Address3">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-3'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/Address4">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-4'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/Address5">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-5'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/PostCode">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'postcode'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/City">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'city'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/US-State">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'state'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/Country">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'country'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/Extended/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="concat('name-', @Type)" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Correspondent/Extended/Type">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="Assignees/Assignee">
      <xsl:variable name="content">
         <xsl:apply-templates select="Name" />
         <xsl:apply-templates select="Address1" mode="concat" />
         <xsl:apply-templates select="Address1" />
         <xsl:apply-templates select="Address2" />
         <xsl:apply-templates select="Address3" />
         <xsl:apply-templates select="Address4" />
         <xsl:apply-templates select="Address5" />
         <xsl:apply-templates select="PostCode" />
         <xsl:apply-templates select="City" />
         <xsl:apply-templates select="US-State" />
         <xsl:apply-templates select="Country" />
         <xsl:apply-templates select="Extended/Name" />
         <xsl:apply-templates select="Extended/Type" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/Address1" mode="concat">
      <!-- Concatenate all address lines into one single line -->
      <xsl:variable name="value">
         <xsl:apply-templates select="self::*" mode="no-breaks" />
         <xsl:if test="following-sibling::Address2">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address2" mode="no-breaks" />
         </xsl:if>
         <xsl:if test="following-sibling::Address3">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address3" mode="no-breaks" />
         </xsl:if>
         <xsl:if test="following-sibling::Address4">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address4" mode="no-breaks" />
         </xsl:if>
         <xsl:if test="following-sibling::Address5">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address5" mode="no-breaks" />
         </xsl:if>
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/Address1">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-1'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/Address2">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-2'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/Address3">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-3'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/Address4">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-4'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/Address5">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-5'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/PostCode">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'postcode'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/City">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'city'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/US-State">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'state'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/Country">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'country'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/Extended/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="concat('name-', @Type)" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignee/Extended/Type">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type-standardized'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="Assignors/Assignor">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ExecutionDate" />
         <xsl:apply-templates select="Name" />
         <xsl:apply-templates select="Address1" mode="concat" />
         <xsl:apply-templates select="Address1" />
         <xsl:apply-templates select="Address2" />
         <xsl:apply-templates select="Address3" />
         <xsl:apply-templates select="Address4" />
         <xsl:apply-templates select="Address5" />
         <xsl:apply-templates select="PostCode" />
         <xsl:apply-templates select="City" />
         <xsl:apply-templates select="US-State" />
         <xsl:apply-templates select="Country" />
         <xsl:apply-templates select="Extended/Name" />
         <xsl:apply-templates select="Extended/Type" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/@ExecutionDate">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'execution-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/Address1" mode="concat">
      <!-- Concatenate all address lines into one single line -->
      <xsl:variable name="value">
         <xsl:apply-templates select="self::*" mode="no-breaks" />
         <xsl:if test="following-sibling::Address2">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address2" mode="no-breaks" />
         </xsl:if>
         <xsl:if test="following-sibling::Address3">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address3" mode="no-breaks" />
         </xsl:if>
         <xsl:if test="following-sibling::Address4">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address4" mode="no-breaks" />
         </xsl:if>
         <xsl:if test="following-sibling::Address5">
            <xsl:text>,</xsl:text>
            <xsl:apply-templates select="following-sibling::Address5" mode="no-breaks" />
         </xsl:if>
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/Address1">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-1'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space($value)" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/Address2">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-2'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/Address3">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-3'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/Address4">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-4'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/Address5">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address-5'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/PostCode">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'postcode'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/City">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'city'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/US-State">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'state'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/Country">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'country'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/Extended/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="concat('name-', @Type)" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Assignor/Extended/Type">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type-standardized'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="ErratumReferences/ErratumReference">
      <xsl:variable name="content">
         <xsl:apply-templates select="Comment" />
         <xsl:apply-templates select="OGDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ErratumReference/Comment">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'comment'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ErratumReference/OGDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'og-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Post-Issuance Data Templates                                                             ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Legal Events Templates                                                                 ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="LegalEvents">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="LegalEvent" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'legal-event'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'legal-events'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvents/LegalEvent">
      <xsl:variable name="content">
         <xsl:apply-templates select="PublicationDate" />
         <xsl:apply-templates select="EventCode1" />
         <xsl:apply-templates select="EventCode2" />
         <xsl:apply-templates select="Effect" />
         <xsl:apply-templates select="StatusIdentifier" />
         <xsl:apply-templates select="DesignatedStateAuthority" />
         <xsl:apply-templates select="DesignatedStateEventCode" />
         <xsl:apply-templates select="DesignatedStates" />
         <xsl:apply-templates select="ExtensionStateAuthority" />
         <xsl:apply-templates select="NewOwner" />
         <xsl:apply-templates select="FreeTextDescription" />
         <xsl:apply-templates select="SPCNumber" />
         <xsl:apply-templates select="FilingDate" />
         <xsl:apply-templates select="ExpiryDate" />
         <xsl:apply-templates select="InventorName" />
         <xsl:apply-templates select="IPC" />
         <xsl:apply-templates select="RepresentativeName" />
         <xsl:apply-templates select="PaymentDate" />
         <xsl:apply-templates select="OpponentName" />
         <xsl:apply-templates select="FeePaymentYear" />
         <xsl:apply-templates select="RequesterName" />
         <xsl:apply-templates select="ExtensionDate" />
         <xsl:apply-templates select="CountriesConcerned" />
         <xsl:apply-templates select="EffectiveDate" />
         <xsl:apply-templates select="WithdrawnDate" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="LegalEvent/PublicationDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'publication-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/EventCode1">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'event-code-1'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/EventCode2">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'event-code-2'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/Effect">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'effect'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/StatusIdentifier">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status-identifier'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/DesignatedStateAuthority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'designated-state-authority'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/DesignatedStateEventCode">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'designated-state-event-code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/DesignatedStates">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'designated-states'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/ExtensionStateAuthority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'extension-state'" />
         <xsl:with-param name="value" select="normalize-space(@Code)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/NewOwner">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'new-owner'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/FreeTextDescription">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'free-text'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/SPCNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'spc-number'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/FilingDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'filing-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/ExpiryDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'expiry-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/InventorName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'inventor'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/IPC">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'ipc'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/RepresentativeName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'representative'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/PaymentDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'payment-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/OpponentName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'opponent'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/FeePaymentYear">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'fee-payment-year'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/RequesterName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'requester'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/ExtensionDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'extension-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/CountriesConcerned">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'countries-concerned'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/EffectiveDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'effective-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LegalEvent/WithdrawnDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'withdrawn-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Legal Events Templates                                                                   ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: National Notifications Templates                                                       ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="NationalNotifications">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:variable name="array">
            <xsl:apply-templates select="NationalNotification" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'notification'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'national-notifications'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="NationalNotifications/NationalNotification">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Lang_ISO639-2" />
         <xsl:apply-templates select="@IssuingOffice" />
         <xsl:apply-templates select="@EventCode" />
         <xsl:apply-templates select="@EventDate" />
         <xsl:apply-templates select="@Status" />
         <xsl:apply-templates select="@SmallEntity" />

         <xsl:variable name="array">
            <xsl:apply-templates select="Notification" />
         </xsl:variable>
         <xsl:call-template name="create-json-element">
            <xsl:with-param name="name" select="'detail'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NationalNotification/@Lang_ISO639-2">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'language'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NationalNotification/@IssuingOffice">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'issuing-office'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NationalNotification/@EventCode">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'event-code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NationalNotification/@EventDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'event-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NationalNotification/@Status">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="NationalNotification/@SmallEntity">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'small-entity'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="NationalNotification/Notification">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="@Code" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: National Notifications Templates                                                         ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: US Pair Data Templates                                                                 ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="USPairData">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="ApplicationNumber" />
         <xsl:apply-templates select="FileCreationDate" />
         <xsl:apply-templates select="XmlCreationDate" />
         <xsl:apply-templates select="PairObtainedDate" />
         <xsl:apply-templates select="ApplicationData" />

         <xsl:variable name="array-1">
            <xsl:apply-templates select="TransactionHistory/History" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'history'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-1)/*" />
         </xsl:call-template>

         <xsl:variable name="array-2">
            <xsl:apply-templates select="FileWrappers/FileWrapper" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'file-wrapper'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-2)/*" />
         </xsl:call-template>

         <xsl:variable name="array-3">
            <xsl:apply-templates select="ForeignPriorities/ForeignPriority" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'foreign-priority'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-3)/*" />
         </xsl:call-template>

         <xsl:apply-templates select="LawFirm" />
         <xsl:apply-templates select="PatentTermAdjustment" />
         <xsl:apply-templates select="PatentTermExtension" />
         <xsl:apply-templates select="ContinuityData" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'us-pair'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="USPairData/ApplicationNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'application-number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="USPairData/FileCreationDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'file-creation-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="USPairData/XmlCreationDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'xml-creation-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="USPairData/PairObtainedDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'pair-obtained-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="USPairData/ApplicationData">
      <xsl:variable name="content">
         <xsl:apply-templates select="Application" />
         <xsl:apply-templates select="ExaminerName" />
         <xsl:apply-templates select="GroupArtUnit" />
         <xsl:apply-templates select="ConfirmationNumber" />
         <xsl:apply-templates select="AttorneyDocketNumber" />
         <xsl:apply-templates select="Classification" />
         <xsl:apply-templates select="FirstNamedInventor" />
         <xsl:apply-templates select="EntityStatus" />
         <xsl:apply-templates select="CustomerNumber" />
         <xsl:apply-templates select="Status" />
         <xsl:apply-templates select="Location" />
         <xsl:apply-templates select="EarliestPublication" />
         <xsl:apply-templates select="Publication" />
         <xsl:apply-templates select="FirstInventorToFile" />
         <xsl:apply-templates select="TitleOfInvention" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'application-data'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/Application" priority="1">
      <xsl:variable name="content">
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Type" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'application'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/Application/Number" priority="1">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/Application/Date" priority="1">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/Application/Type" priority="1">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/ExaminerName">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'examiner-name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/GroupArtUnit">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'group-art-unit'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/ConfirmationNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'confirmation-number'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/AttorneyDocketNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'attorney-docket-number'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/Classification">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'classification'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'classification-type'" />
         <xsl:with-param name="value" select="normalize-space(@Type)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/FirstNamedInventor">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'first-named-inventor'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/EntityStatus">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'entity-status'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/CustomerNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'customer-number'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/Status">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status-date'" />
         <xsl:with-param name="value" select="normalize-space(@Date)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/Location">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'location'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'location-date'" />
         <xsl:with-param name="value" select="normalize-space(@Date)" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/EarliestPublication">
      <xsl:variable name="content">
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'earliest-publication'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/EarliestPublication/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/EarliestPublication/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/Publication" priority="1">
      <xsl:variable name="content">
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'publication'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/Publication/Number" priority="1">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/Publication/Date" priority="1">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/FirstInventorToFile">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'first-inventor-to-file'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ApplicationData/TitleOfInvention">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'title-of-invention'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="TransactionHistory/History">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Date" />
         <xsl:apply-templates select="text()" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="TransactionHistory/History/@Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="TransactionHistory/History/text()">
      <xsl:variable name="value">
         <xsl:apply-templates select="self::text()" mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'#text'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="FileWrappers/FileWrapper">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Date" />
         <xsl:apply-templates select="@PageCount" />
         <xsl:apply-templates select="@Filename" />
         <xsl:apply-templates select="@Key" />
         <xsl:apply-templates select="DocumentCode" />
         <xsl:apply-templates select="Category" />
         <xsl:apply-templates select="SearchAbstract" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FileWrapper/@Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FileWrapper/@PageCount">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'page-count'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FileWrapper/@Filename">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'filename'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space()" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FileWrapper/@Key">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'key'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FileWrapper/DocumentCode">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'document-code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FileWrapper/Category">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'category'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="FileWrapper/SearchAbstract">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'search-abstract'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="ForeignPriorities/ForeignPriority">
      <xsl:variable name="content">
         <xsl:apply-templates select="Authority" />
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ForeignPriority/Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ForeignPriority/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="ForeignPriority/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="USPairData/LawFirm">
      <xsl:variable name="content">
         <xsl:apply-templates select="CorrespondenceAddress" />

         <xsl:variable name="array">
            <xsl:apply-templates select="AttorneyAgents/AttorneyAgent" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'attorney-agent'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'law-firm'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LawFirm/CorrespondenceAddress">
      <xsl:variable name="content">
         <xsl:apply-templates select="Name" />
         <xsl:apply-templates select="Address" />
         <xsl:apply-templates select="CustomerNumber" />
         <xsl:apply-templates select="Extended/Name" />
         <xsl:apply-templates select="Extended/Type" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'correspondence-address'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LawFirm/CorrespondenceAddress/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LawFirm/CorrespondenceAddress/Address">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'address'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LawFirm/CorrespondenceAddress/CustomerNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'customer-number'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="CorrespondenceAddress/Extended/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="concat('name-', @Type)" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="CorrespondenceAddress/Extended/Type">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type-standardized'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="LawFirm/AttorneyAgents/AttorneyAgent">
      <xsl:variable name="content">
         <xsl:apply-templates select="Name" />
         <xsl:apply-templates select="RegistrationNumber" />
         <xsl:apply-templates select="Phone" />
         <xsl:apply-templates select="Extended/Name" />
         <xsl:apply-templates select="Extended/Type" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AttorneyAgents/AttorneyAgent/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="no-breaks" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AttorneyAgents/AttorneyAgent/RegistrationNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'registration-number'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AttorneyAgents/AttorneyAgent/Phone">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'phone'" />
         <xsl:with-param name="value">
            <xsl:apply-templates mode="text" />
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AttorneyAgent/Extended/Name">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="concat('name-', @Type)" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="AttorneyAgent/Extended/Type">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type-standardized'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="USPairData/PatentTermAdjustment">
      <xsl:variable name="content">
         <xsl:apply-templates select="FilingDate" />
         <xsl:apply-templates select="IssueDate" />
         <xsl:apply-templates select="DelaysA" />
         <xsl:apply-templates select="DelaysB" />
         <xsl:apply-templates select="DelaysC" />
         <xsl:apply-templates select="OverlappingDays" />
         <xsl:apply-templates select="NonOverlappingDelaysUSPTO" />
         <xsl:apply-templates select="ManualAdjustmentsPTO" />
         <xsl:apply-templates select="ApplicantDelays" />
         <xsl:apply-templates select="TotalAdjustmentsPTA" />

         <xsl:variable name="array">
            <xsl:apply-templates select="History/Entry" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'history'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'patent-term-adjustment'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/FilingDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'filing-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/IssueDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'issue-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/DelaysA">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'delays-a'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/DelaysB">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'delays-b'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/DelaysC">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'delays-c'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/OverlappingDays">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'overlapping-days'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/NonOverlappingDelaysUSPTO">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'non-overlapping-delays-uspto'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/ManualAdjustmentsPTO">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'manual-adjustments-pto'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/ApplicantDelays">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'applicant-delays'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/TotalAdjustmentsPTA">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'total-adjustments-pta'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="PatentTermAdjustment/History/Entry">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Number" />
         <xsl:apply-templates select="@Date" />
         <xsl:apply-templates select="@DaysPTO" />
         <xsl:apply-templates select="@DaysAppl" />
         <xsl:apply-templates select="@Start" />
         <xsl:apply-templates select="text()" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/History/Entry/@Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/History/Entry/@Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/History/Entry/@DaysPTO">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'days-pto'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/History/Entry/@DaysAppl">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'days-appl'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/History/Entry/@Start">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'start'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermAdjustment/History/Entry/text()">
      <xsl:variable name="value">
         <xsl:apply-templates select="self::text()" mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'#text'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="USPairData/PatentTermExtension">
      <xsl:variable name="content">
         <xsl:apply-templates select="FilingDate" />
         <xsl:apply-templates select="ManualAdjustmentsPTO" />
         <xsl:apply-templates select="NonOverlappingDelaysUSPTO" />
         <xsl:apply-templates select="CorrectionDelay" />
         <xsl:apply-templates select="TotalExtensionsPTE" />

         <xsl:variable name="array">
            <xsl:apply-templates select="History/Entry" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'history'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'patent-term-extension'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermExtension/FilingDate">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'filing-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermExtension/ManualAdjustmentsPTO">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'manual-adjustments-pto'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermExtension/NonOverlappingDelaysUSPTO">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'non-overlapping-delays-uspto'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermExtension/CorrectionDelay">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'correction-delay'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermExtension/TotalExtensionsPTE">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'total-extensions-pte'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="PatentTermExtension/History/Entry">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Date" />
         <xsl:apply-templates select="@DaysPTO" />
         <xsl:apply-templates select="@DaysAppl" />
         <xsl:apply-templates select="text()" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermExtension/History/Entry/@Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermExtension/History/Entry/@DaysPTO">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'days-pto'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermExtension/History/Entry/@DaysAppl">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'days-appl'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PatentTermExtension/History/Entry/text()">
      <xsl:variable name="value">
         <xsl:apply-templates select="self::text()" mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'#text'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="USPairData/ContinuityData">
      <xsl:variable name="content">
         <xsl:variable name="array-1">
            <xsl:apply-templates select="Parents/Parent" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'parent'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-1)/*" />
         </xsl:call-template>

         <xsl:variable name="array-2">
            <xsl:apply-templates select="Childs/Child" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'child'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array-2)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'continuity-data'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="ContinuityData/Parents/Parent">
      <xsl:variable name="content">
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Status" />
         <xsl:apply-templates select="PatentNumber" />
         <xsl:apply-templates select="Text" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Parents/Parent/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Parents/Parent/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Parents/Parent/Status">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Parents/Parent/PatentNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'patent-number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Parents/Parent/Text">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'text'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="ContinuityData/Childs/Child">
      <xsl:variable name="content">
         <xsl:apply-templates select="Number" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Status" />
         <xsl:apply-templates select="ApplicationNumber" />
         <xsl:apply-templates select="Text" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Childs/Child/Number">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Childs/Child/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Childs/Child/Status">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'status'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Childs/Child/ApplicationNumber">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'application-number'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Childs/Child/Text">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'text'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: US Pair Data Templates                                                                   ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Prime Ntified Events Templates                                                         ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="PrimeNotifiedEvents" mode="status">
      <xsl:apply-templates select="PrimeNotifiedSection[@Section = 'Status']" mode="status" />
   </xsl:template>
   <xsl:template match="PrimeNotifiedSection" mode="status">
      <xsl:apply-templates select="PrimeNotifiedEvent" mode="status" />
   </xsl:template>
   <xsl:template match="PrimeNotifiedEvent[@Event = 'Ceased']" priority="1" mode="status">
      <Status Name="{@Event}" Date="{Date}" Indicator="-" />
   </xsl:template>
   <xsl:template match="PrimeNotifiedEvent[@Event = 'Reinstated']" priority="1" mode="status">
      <Status Name="{@Event}" Date="{Date}" Indicator="?" />
   </xsl:template>
   <xsl:template match="PrimeNotifiedEvent" mode="status">
      <Status Name="{@Event}" Date="{Date}" Indicator="+" />
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="PrimeNotifiedEvents">
      <xsl:variable name="content">
         <xsl:variable name="array">
            <xsl:apply-templates select="PrimeNotifiedSection" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'section'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'prime-notified-events'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="PrimeNotifiedEvents/PrimeNotifiedSection[@Section = 'Blocked']" priority="1" />
   <xsl:template match="PrimeNotifiedEvents/PrimeNotifiedSection">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Section" />

         <xsl:variable name="array">
            <xsl:apply-templates select="PrimeNotifiedEvent" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'event'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimeNotifiedEvents/PrimeNotifiedSection/@Section">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="PrimeNotifiedSection/PrimeNotifiedEvent">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Event" />
         <xsl:apply-templates select="@Authority" />
         <xsl:apply-templates select="@Code" />
         <xsl:apply-templates select="Date" />
         <xsl:apply-templates select="Details/Detail" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimeNotifiedSection/PrimeNotifiedEvent/@Event">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'name'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimeNotifiedSection/PrimeNotifiedEvent/@Authority">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'authority'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimeNotifiedSection/PrimeNotifiedEvent/@Code">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'code'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimeNotifiedSection/PrimeNotifiedEvent/Date[@Type]" priority="1">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="concat('date-', @Type)" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimeNotifiedSection/PrimeNotifiedEvent/Date">
      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimeNotifiedEvent/Details/Detail[@Type]" priority="1">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="concat('detail-', @Type)" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="PrimeNotifiedEvent/Details/Detail">
      <xsl:variable name="value">
         <xsl:apply-templates mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'detail'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="$value" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Prime Notified Events Templates                                                          ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Drawings & Images Templates                                                            ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="Drawings">
      <xsl:variable name="content">
         <xsl:apply-templates select="@DrawingsId" />

         <xsl:variable name="array">
            <xsl:apply-templates select="figure" />
         </xsl:variable>
         <xsl:call-template name="create-json-array">
            <xsl:with-param name="name" select="'figure'" />
            <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'drawings'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Drawings/@DrawingsId">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'id'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Drawings/figure">
      <xsl:variable name="content">
         <xsl:apply-templates select="@*" />
         <xsl:apply-templates select="img" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="figure/@*">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="name()" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="figure/img">
      <xsl:variable name="content">
         <xsl:apply-templates select="@*" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'img'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="img/@*">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="name()" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Images">
      <xsl:variable name="content">
         <xsl:apply-templates select="@ChangeDate" />

         <xsl:apply-templates select="PDFs" />
         <xsl:apply-templates select="ClippedImages" />
         <xsl:apply-templates select="DrawingSheets" />
         <xsl:apply-templates select="InlineImages" />
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'images'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Images/PDFs">
      <xsl:variable name="array">
         <xsl:apply-templates select="Image" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'pdf'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Images/ClippedImages">
      <xsl:variable name="array">
         <xsl:apply-templates select="Image" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'clipped-image'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Images/DrawingSheets">
      <xsl:variable name="array">
         <xsl:apply-templates select="Image" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'drawing-sheet'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Images/InlineImages">
      <xsl:variable name="array">
         <xsl:apply-templates select="Image" />
      </xsl:variable>

      <xsl:call-template name="create-json-array">
         <xsl:with-param name="name" select="'inline-image'" />
         <xsl:with-param name="nodes" select="exsl:node-set($array)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="Images/*/Image">
      <xsl:variable name="content">
         <xsl:apply-templates select="@Key" />
         <xsl:apply-templates select="@ChainSequence" />
         <xsl:apply-templates select="@Format" />
         <xsl:apply-templates select="@PageCount" />
         <xsl:apply-templates select="@FileSize" />
         <xsl:apply-templates select="@Filename" />
         <xsl:apply-templates select="@SourceFilename" />
         <xsl:apply-templates select="@SizeFormat" />
         <xsl:apply-templates select="@SubType" />
      </xsl:variable>

      <xsl:call-template name="create-json-object">
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Image/@Key">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'key'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Image/@ChainSequence">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'sequence'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Image/@Format">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'format'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Image/@PageCount">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'pages'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Image/@FileSize">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'size'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Image/@SizeFormat">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'type'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Image/@SubType">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'subtype'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Image/@Filename">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'filename'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space()" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Image/@SourceFilename">
      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'source-filename'" />
         <xsl:with-param name="value">
            <xsl:call-template name="escapeJSON">
               <xsl:with-param name="text" select="normalize-space()" />
            </xsl:call-template>         
         </xsl:with-param>
      </xsl:call-template>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Drawings & Images Templates                                                              ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: HTML Rendering Templates                                                               ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="p/@num">
      <xsl:choose>
         <xsl:when test="number() = n/a">
            <!-- Skip -->
         </xsl:when>
         <xsl:when test="number() = 0">
            <!-- Skip -->
         </xsl:when>
         <xsl:otherwise>
            <xsl:text>[</xsl:text>
            <xsl:value-of select="format-number(., '0000')" />
            <xsl:text>] </xsl:text>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="p">
      <p>
         <xsl:apply-templates select="@num" />
         <xsl:apply-templates />
      </p>
   </xsl:template>

   <xsl:template match="heading">
      <xsl:variable name="level" select="@level" />

      <xsl:choose>
         <xsl:when test="$level and contains('1|2|3|4|5|6', $level)">
            <xsl:element name="{concat('h', $level)}">
               <xsl:apply-templates />
            </xsl:element>
         </xsl:when>
         <xsl:otherwise>
            <h1>
               <xsl:apply-templates />
            </h1>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template match="chemistry">
      <!-- Skip <chem> element, only keep <img> element -->
      <xsl:apply-templates select="img" />
   </xsl:template>

   <xsl:template match="maths">
      <!-- Skip <math> element, only keep <img> element -->
      <xsl:apply-templates select="img" />
   </xsl:template>

   <xsl:template match="tables">
      <xsl:apply-templates select="table | img" />
   </xsl:template>

   <xsl:template match="img">
      <!--<img src="{concat('img/', @file)}">-->
      <img src="{@file}">
         <xsl:copy-of select="@id | @alt | @img-content" />
      </img>
   </xsl:template>

   <xsl:template match="b">
      <b>
         <xsl:apply-templates />
      </b>
   </xsl:template>
   <xsl:template match="i">
      <i>
         <xsl:apply-templates />
      </i>
   </xsl:template>
   <xsl:template match="u">
      <u>
         <xsl:apply-templates select="@style" />
         <xsl:apply-templates />
      </u>
   </xsl:template>
   <xsl:template match="sub">
      <sub>
         <xsl:apply-templates />
      </sub>
   </xsl:template>
   <xsl:template match="sup">
      <sup>
         <xsl:apply-templates />
      </sup>
   </xsl:template>
   <xsl:template match="smallcaps">
      <small>
         <xsl:apply-templates />
      </small>
   </xsl:template>

   <xsl:template match="ol">
      <ol>
         <!--<xsl:apply-templates select="@style" />-->
         <xsl:apply-templates />
      </ol>
   </xsl:template>
   <xsl:template match="ul">
      <ul>
         <!--<xsl:apply-templates select="@style" />-->
         <xsl:apply-templates />
      </ul>
   </xsl:template>
   <xsl:template match="li">
      <li>
         <xsl:apply-templates />
      </li>
   </xsl:template>

   <xsl:template match="dl">
      <dl>
         <xsl:apply-templates />
      </dl>
   </xsl:template>
   <xsl:template match="dt">
      <dt>
         <xsl:apply-templates />
      </dt>
   </xsl:template>
   <xsl:template match="dd">
      <dd>
         <xsl:apply-templates />
      </dd>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: HTML Rendering Templates                                                                 ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: CALS Table Markup Templates                                                            ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="table">
      <xsl:apply-templates />
   </xsl:template>

   <xsl:template match="table/title" />

   <xsl:template match="tgroup">
      <table>
         <xsl:if test="../@pgwide=1">
            <xsl:attribute name="width">100%</xsl:attribute>
         </xsl:if>

         <xsl:if test="@align">
            <xsl:attribute name="align">
               <xsl:value-of select="@align"/>
            </xsl:attribute>
         </xsl:if>

         <xsl:variable name="table.frame" select="translate(../@frame, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" />
         <xsl:choose>
            <xsl:when test="$table.frame = 'top'">
               <xsl:attribute name="style">border-top:thin solid black</xsl:attribute>
            </xsl:when>
            <xsl:when test="$table.frame = 'bottom'">
               <xsl:attribute name="style">border-bottom:thin solid black</xsl:attribute>
            </xsl:when>
            <xsl:when test="$table.frame = 'topbot'">
               <xsl:attribute name="style">border-top:thin solid black;border-bottom:thin solid black</xsl:attribute>
            </xsl:when>
            <xsl:when test="$table.frame = 'all'">
               <xsl:attribute name="style">border:thin solid black</xsl:attribute>
            </xsl:when>
            <xsl:when test="$table.frame = 'sides'">
               <xsl:attribute name="style">border-left:thin solid black;border-right:thin solid black</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
               <xsl:attribute name="border">0</xsl:attribute>
               <!--<xsl:attribute name="border">1</xsl:attribute>-->
            </xsl:otherwise>
         </xsl:choose>

         <xsl:variable name="colspec.totalwidth">
            <xsl:call-template name="calculate.totalwidth">
               <xsl:with-param name="colspecs" select="colspec" />
            </xsl:call-template>
         </xsl:variable>

         <xsl:variable name="colgroup">
            <colgroup>
               <xsl:call-template name="generate.colgroup">
                  <xsl:with-param name="cols" select="@cols"/>
                  <xsl:with-param name="totalwidth" select="$colspec.totalwidth" />
               </xsl:call-template>
            </colgroup>
         </xsl:variable>

         <xsl:variable name="table.width">
            <xsl:choose>
               <xsl:when test="$default.table.width = ''">
                  <xsl:text>100%</xsl:text>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="$default.table.width"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:variable>

         <xsl:attribute name="width">
            <xsl:value-of select="$table.width"/>
         </xsl:attribute>

         <xsl:if test="../title">
            <caption>
               <xsl:value-of select="../title" />
            </caption>
         </xsl:if>


         <xsl:copy-of select="$colgroup"/>

         <xsl:apply-templates/>

      </table>
   </xsl:template>

   <xsl:template match="colspec"></xsl:template>

   <xsl:template match="spanspec"></xsl:template>

   <xsl:template match="thead | tfoot">
      <xsl:element name="{name(.)}">
         <xsl:if test="@align">
            <xsl:attribute name="align">
               <xsl:value-of select="@align"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@char">
            <xsl:attribute name="char">
               <xsl:value-of select="@char"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@charoff">
            <xsl:attribute name="charoff">
               <xsl:value-of select="@charoff"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@valign">
            <xsl:attribute name="valign">
               <xsl:value-of select="@valign"/>
            </xsl:attribute>
         </xsl:if>

         <xsl:apply-templates/>
      </xsl:element>
   </xsl:template>

   <xsl:template match="tbody">
      <tbody>
         <xsl:if test="@align">
            <xsl:attribute name="align">
               <xsl:value-of select="@align"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@char">
            <xsl:attribute name="char">
               <xsl:value-of select="@char"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@charoff">
            <xsl:attribute name="charoff">
               <xsl:value-of select="@charoff"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@valign">
            <xsl:attribute name="valign">
               <xsl:value-of select="@valign"/>
            </xsl:attribute>
         </xsl:if>

         <xsl:apply-templates/>
      </tbody>
   </xsl:template>

   <xsl:template match="row">
      <tr>
         <xsl:if test="@align">
            <xsl:attribute name="align">
               <xsl:value-of select="@align"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@char">
            <xsl:attribute name="char">
               <xsl:value-of select="@char"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@charoff">
            <xsl:attribute name="charoff">
               <xsl:value-of select="@charoff"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@valign">
            <xsl:attribute name="valign">
               <xsl:value-of select="@valign"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:apply-templates/>
      </tr>
   </xsl:template>

   <xsl:template match="thead/row/entry">
      <xsl:call-template name="process.cell">
         <xsl:with-param name="cellgi">th</xsl:with-param>
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="tbody/row/entry">
      <xsl:call-template name="process.cell">
         <xsl:with-param name="cellgi">td</xsl:with-param>
      </xsl:call-template>
   </xsl:template>

   <xsl:template match="tfoot/row/entry">
      <xsl:call-template name="process.cell">
         <xsl:with-param name="cellgi">th</xsl:with-param>
      </xsl:call-template>
   </xsl:template>

   <xsl:template name="process.cell">
      <xsl:param name="cellgi">td</xsl:param>

      <xsl:variable name="empty.cell" select="count(node()) = 0"/>

      <xsl:variable name="entry.colnum">
         <xsl:call-template name="entry.colnum"/>
      </xsl:variable>

      <xsl:element name="{$cellgi}">

         <xsl:if test="@spanname">
            <xsl:variable name="namest" select="ancestor::tgroup/spanspec[@spanname=./@spanname]/@namest"/>
            <xsl:variable name="nameend" select="ancestor::tgroup/spanspec[@spanname=./@spanname]/@nameend"/>
            <xsl:variable name="colst" select="ancestor::*[colspec/@colname=$namest]/colspec[@colname=$namest]/@colnum"/>
            <xsl:variable name="colend" select="ancestor::*[colspec/@colname=$nameend]/colspec[@colname=$nameend]/@colnum"/>
            <xsl:attribute name="colspan">
               <xsl:value-of select="number($colend) - number($colst) + 1"/>
            </xsl:attribute>
         </xsl:if>

         <xsl:if test="@morerows">
            <xsl:attribute name="rowspan">
               <xsl:value-of select="@morerows+1"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@namest">
            <xsl:attribute name="colspan">
               <xsl:call-template name="calculate.colspan"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@align">
            <xsl:attribute name="align">
               <xsl:value-of select="@align"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@char">
            <xsl:attribute name="char">
               <xsl:value-of select="@char"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@charoff">
            <xsl:attribute name="charoff">
               <xsl:value-of select="@charoff"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:if test="@valign">
            <xsl:attribute name="valign">
               <xsl:value-of select="@valign"/>
            </xsl:attribute>
         </xsl:if>

         <xsl:if test="@rowsep='1'">
            <xsl:attribute name="style">border-bottom:thin solid black</xsl:attribute>
         </xsl:if>

         <xsl:if test="not(preceding-sibling::*) and ancestor::row/@id">
            <a name="{ancestor::row/@id}"/>
         </xsl:if>

         <xsl:if test="@id">
            <a name="{@id}"/>
         </xsl:if>

         <xsl:choose>
            <xsl:when test="$empty.cell">
               <xsl:text>&#160;</xsl:text>
            </xsl:when>
            <xsl:otherwise>
               <xsl:apply-templates/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:element>
   </xsl:template>

   <xsl:template name="entry.colnum">
      <xsl:param name="entry" select="."/>

      <xsl:choose>
         <xsl:when test="$entry/@colname">
            <xsl:variable name="colname" select="$entry/@colname"/>
            <xsl:variable name="colspec" select="$entry/ancestor::tgroup/colspec[@colname=$colname]"/>
            <xsl:call-template name="colspec.colnum">
               <xsl:with-param name="colspec" select="$colspec"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="$entry/@namest">
            <xsl:variable name="namest" select="$entry/@namest"/>
            <xsl:variable name="colspec" select="$entry/ancestor::tgroup/colspec[@colname=$namest]"/>
            <xsl:call-template name="colspec.colnum">
               <xsl:with-param name="colspec" select="$colspec"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="count($entry/preceding-sibling::*) = 0">1</xsl:when>
         <xsl:otherwise>
            <xsl:variable name="pcol">
               <xsl:call-template name="entry.ending.colnum">
                  <xsl:with-param name="entry" select="$entry/preceding-sibling::*[1]"/>
               </xsl:call-template>
            </xsl:variable>
            <xsl:value-of select="$pcol + 1"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="entry.ending.colnum">
      <xsl:param name="entry" select="."/>

      <xsl:choose>
         <xsl:when test="$entry/@colname">
            <xsl:variable name="colname" select="$entry/@colname"/>
            <xsl:variable name="colspec" select="$entry/ancestor::tgroup/colspec[@colname=$colname]"/>
            <xsl:call-template name="colspec.colnum">
               <xsl:with-param name="colspec" select="$colspec"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="$entry/@nameend">
            <xsl:variable name="nameend" select="$entry/@nameend"/>
            <xsl:variable name="colspec" select="$entry/ancestor::tgroup/colspec[@colname=$nameend]"/>
            <xsl:call-template name="colspec.colnum">
               <xsl:with-param name="colspec" select="$colspec"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="count($entry/preceding-sibling::*) = 0">1</xsl:when>
         <xsl:otherwise>
            <xsl:variable name="pcol">
               <xsl:call-template name="entry.ending.colnum">
                  <xsl:with-param name="entry" select="$entry/preceding-sibling::*[1]"/>
               </xsl:call-template>
            </xsl:variable>
            <xsl:value-of select="$pcol + 1"/>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="colspec.colnum">
      <xsl:param name="colspec" select="."/>
      <xsl:choose>
         <xsl:when test="$colspec/@colnum">
            <xsl:value-of select="$colspec/@colnum"/>
         </xsl:when>
         <xsl:when test="$colspec/preceding-sibling::colspec">
            <xsl:variable name="prec.colspec.colnum">
               <xsl:call-template name="colspec.colnum">
                  <xsl:with-param name="colspec"
                                  select="$colspec/preceding-sibling::colspec[1]"/>
               </xsl:call-template>
            </xsl:variable>
            <xsl:value-of select="$prec.colspec.colnum + 1"/>
         </xsl:when>
         <xsl:otherwise>1</xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="generate.colgroup">
      <xsl:param name="cols" select="1" />
      <xsl:param name="count" select="1" />
      <xsl:param name="totalwidth" />

      <xsl:choose>
         <xsl:when test="$count &gt; $cols"></xsl:when>
         <xsl:otherwise>
            <xsl:call-template name="generate.col">
               <xsl:with-param name="countcol" select="$count"/>
               <xsl:with-param name="totalwidth" select="$totalwidth" />
            </xsl:call-template>
            <xsl:call-template name="generate.colgroup">
               <xsl:with-param name="cols" select="$cols"/>
               <xsl:with-param name="count" select="$count + 1"/>
               <xsl:with-param name="totalwidth" select="$totalwidth" />
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="generate.col">
      <xsl:param name="countcol">1</xsl:param>
      <xsl:param name="colspecs" select="./colspec"/>
      <xsl:param name="count">1</xsl:param>
      <xsl:param name="colnum">1</xsl:param>
      <xsl:param name="totalwidth" />

      <xsl:choose>
         <xsl:when test="$count &gt; count($colspecs)">
            <col />
         </xsl:when>
         <xsl:otherwise>
            <xsl:variable name="colspec" select="$colspecs[$count = position()]"/>
            <xsl:variable name="colspec.colnum">
               <xsl:choose>
                  <xsl:when test="$colspec/@colnum">
                     <xsl:value-of select="$colspec/@colnum"/>
                  </xsl:when>
                  <xsl:otherwise>
                     <xsl:value-of select="$colnum"/>
                  </xsl:otherwise>
               </xsl:choose>
            </xsl:variable>

            <xsl:choose>
               <xsl:when test="$colspec.colnum = $countcol">
                  <col>
                     <xsl:if test="$colspec/@align">
                        <xsl:attribute name="align">
                           <xsl:value-of select="$colspec/@align"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="$colspec/@char">
                        <xsl:attribute name="char">
                           <xsl:value-of select="$colspec/@char"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="$colspec/@charoff">
                        <xsl:attribute name="charoff">
                           <xsl:value-of select="$colspec/@charoff"/>
                        </xsl:attribute>
                     </xsl:if>
                     <xsl:if test="$totalwidth &gt; 0 and $colspec/@colwidth">
                        <xsl:variable name="colwidth" select="translate($colspec/@colwidth, 'anPTX', 'anptx')" />
                        <xsl:choose>
                           <xsl:when test="contains($colwidth, '%')">
                              <xsl:attribute name="width">
                                 <xsl:value-of select="$colwidth" />
                              </xsl:attribute>
                           </xsl:when>
                           <xsl:when test="contains($colwidth, 'mm')">
                              <xsl:variable name="mm" select="substring-before($colwidth, 'mm')" />
                              <xsl:attribute name="width">
                                 <xsl:value-of select="format-number((100 * number($mm)) div $totalwidth, '0.00')" />
                                 <xsl:text>%</xsl:text>
                              </xsl:attribute>
                           </xsl:when>
                           <xsl:when test="contains($colwidth, 'pt')">
                              <xsl:variable name="pt" select="substring-before($colwidth, 'pt')" />
                              <xsl:attribute name="width">
                                 <xsl:value-of select="format-number((100 * number($pt)) div $totalwidth, '0.00')"/>
                                 <xsl:text>%</xsl:text>
                              </xsl:attribute>
                           </xsl:when>
                           <xsl:when test="contains($colwidth, 'px')">
                              <xsl:variable name="px" select="substring-before($colwidth, 'px')" />
                              <xsl:attribute name="width">
                                 <xsl:value-of select="format-number((100 * number($px)) div $totalwidth, '0.00')"/>
                                 <xsl:text>%</xsl:text>
                              </xsl:attribute>
                           </xsl:when>
                        </xsl:choose>
                     </xsl:if>
                  </col>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:call-template name="generate.col">
                     <xsl:with-param name="countcol" select="$countcol"/>
                     <xsl:with-param name="colspecs" select="$colspecs"/>
                     <xsl:with-param name="count" select="$count + 1"/>
                     <xsl:with-param name="colnum">
                        <xsl:choose>
                           <xsl:when test="$colspec/@colnum">
                              <xsl:value-of select="$colspec/@colnum + 1"/>
                           </xsl:when>
                           <xsl:otherwise>
                              <xsl:value-of select="$colnum + 1"/>
                           </xsl:otherwise>
                        </xsl:choose>
                     </xsl:with-param>
                     <xsl:with-param name="totalwidth" select="$totalwidth" />
                  </xsl:call-template>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:otherwise>
      </xsl:choose>

   </xsl:template>

   <xsl:template name="colspec.colwidth">
      <!-- when this macro is called, the current context must be an entry -->
      <xsl:param name="colname"></xsl:param>
      <!-- .. = row, ../.. = thead|tbody, ../../.. = tgroup -->
      <xsl:param name="colspecs" select="../../../../tgroup/colspec"/>
      <xsl:param name="count">1</xsl:param>

      <xsl:choose>
         <xsl:when test="$count &gt; count($colspecs)"></xsl:when>
         <xsl:otherwise>
            <xsl:variable name="colspec" select="$colspecs[$count = position()]"/>
            <xsl:choose>
               <xsl:when test="$colspec/@colname = $colname">
                  <xsl:value-of select="$colspec/@colwidth"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:call-template name="colspec.colwidth">
                     <xsl:with-param name="colname" select="$colname"/>
                     <xsl:with-param name="colspecs" select="$colspecs"/>
                     <xsl:with-param name="count" select="$count + 1"/>
                  </xsl:call-template>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>

   <xsl:template name="calculate.totalwidth">
      <xsl:param name="total" select="0" />
      <xsl:param name="index" select="1" />
      <xsl:param name="colspecs" />
      <xsl:param name="metrics" />

      <xsl:variable name="colwidth" select="translate($colspecs[$index]/@colwidth, 'anPTX', 'anptx')" />
      <xsl:choose>
         <xsl:when test="$index &gt; count($colspecs)">
            <!-- done, return total width -->
            <xsl:value-of select="$total" />
         </xsl:when>
         <xsl:when test="$colwidth = 'n/a'">
            <xsl:value-of select="0" />
         </xsl:when>
         <xsl:when test="$metrics and contains($colwidth, $metrics)">
            <!-- not all columns are specified in the same metrics -->
            <xsl:value-of select="0" />
         </xsl:when>
         <xsl:when test="contains($colwidth, 'mm')">
            <xsl:variable name="mm" select="number(substring-before($colwidth, 'mm'))" />
            <xsl:call-template name="calculate.totalwidth">
               <xsl:with-param name="total" select="$total + $mm" />
               <xsl:with-param name="index" select="$index + 1" />
               <xsl:with-param name="colspecs" select="$colspecs" />
               <xsl:with-param name="metrics" select="'mm'" />
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($colwidth, 'pt')">
            <xsl:variable name="pt" select="number(substring-before($colwidth, 'pt'))" />
            <xsl:call-template name="calculate.totalwidth">
               <xsl:with-param name="total" select="$total + $pt" />
               <xsl:with-param name="index" select="$index + 1" />
               <xsl:with-param name="colspecs" select="$colspecs" />
               <xsl:with-param name="metrics" select="'pt'" />
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="contains($colwidth, 'px')">
            <xsl:variable name="px" select="number(substring-before($colwidth, 'px'))" />
            <xsl:call-template name="calculate.totalwidth">
               <xsl:with-param name="total" select="$total + $px" />
               <xsl:with-param name="index" select="$index + 1" />
               <xsl:with-param name="colspecs" select="$colspecs" />
               <xsl:with-param name="metrics" select="'px'" />
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <!-- column specified in unsupported metrics (or percentage) -->
            <xsl:value-of select="0" />
         </xsl:otherwise>
      </xsl:choose>

   </xsl:template>

   <xsl:template name="calculate.colspan">
      <xsl:param name="entry" select="."/>

      <xsl:variable name="namest" select="$entry/@namest"/>
      <xsl:variable name="nameend" select="$entry/@nameend"/>

      <xsl:variable name="scol">
         <xsl:call-template name="colspec.colnum">
            <xsl:with-param name="colspec" select="$entry/ancestor::tgroup/colspec[@colname = $namest]"/>
         </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="ecol">
         <xsl:call-template name="colspec.colnum">
            <xsl:with-param name="colspec" select="$entry/ancestor::tgroup/colspec[@colname=$nameend]"/>
         </xsl:call-template>
      </xsl:variable>
      <xsl:value-of select="$ecol - $scol + 1"/>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: CALS Table Markup Templates                                                              ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Common Templates                                                                       ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template match="@InsertDate">
      <xsl:text xml:space="preserve" disable-output-escaping="yes">,</xsl:text>

      <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'insert-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="@InsertTime">
      <xsl:text xml:space="preserve" disable-output-escaping="yes">,</xsl:text>

         <xsl:call-template name="create-json-time-field">
         <xsl:with-param name="name" select="'insert-time'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   
   <xsl:template match="@ChangeDate">
      <xsl:text xml:space="preserve" disable-output-escaping="yes">,</xsl:text>

   <xsl:call-template name="create-json-date-field">
         <xsl:with-param name="name" select="'change-date'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="@ChangeTime">
      <xsl:text xml:space="preserve" disable-output-escaping="yes">,</xsl:text>

         <xsl:call-template name="create-json-time-field">
         <xsl:with-param name="name" select="'change-time'" />
         <xsl:with-param name="value" select="normalize-space()" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="@Equivalent">
      <xsl:variable name="content">
         <xsl:call-template name="create-json-field">
            <xsl:with-param name="name" select="'authority'" />
            <xsl:with-param name="value" select="normalize-space(parent::*/@Authority)" />
         </xsl:call-template>
         <xsl:call-template name="create-json-field">
            <xsl:with-param name="name" select="'number'" />
            <xsl:with-param name="value" select="normalize-space(parent::*/@Number)" />
         </xsl:call-template>
         <xsl:call-template name="create-json-field">
            <xsl:with-param name="name" select="'kind'" />
            <xsl:with-param name="value" select="normalize-space(parent::*/@Kind)" />
         </xsl:call-template>
         <xsl:call-template name="create-json-field">
            <xsl:with-param name="name" select="'generated'" />
            <xsl:with-param name="value" select="normalize-space(parent::*/@Generated)" />
         </xsl:call-template>
      </xsl:variable>

      <xsl:call-template name="create-json-element">
         <xsl:with-param name="name" select="'equivalent'" />
         <xsl:with-param name="nodes" select="exsl:node-set($content)/*" />
      </xsl:call-template>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="node()" mode="no-breaks">
      <xsl:if test="preceding-sibling::node()">
         <xsl:text> </xsl:text>
      </xsl:if>

      <xsl:if test="normalize-space()">
         <xsl:copy />
      </xsl:if>
   </xsl:template>
   <xsl:template match="*" mode="no-breaks" priority="1">
      <xsl:if test="preceding-sibling::node()">
         <xsl:text> </xsl:text>
      </xsl:if>

      <xsl:apply-templates mode="no-breaks" />
   </xsl:template>
   <xsl:template match="br" mode="no-breaks" priority="2">
      <xsl:if test="following-sibling::node()">
         <xsl:text>,</xsl:text>
      </xsl:if>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="*" mode="html">
      <!-- Remove subsections from Description -->
      <xsl:apply-templates select="*" mode="html" />
   </xsl:template>
   <xsl:template match="Title" mode="html" priority="1">
      <xsl:variable name="html">
         <!-- Contents of element -->
         <xsl:apply-templates select="node()" mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'#text'" />
         <xsl:with-param name="value" select="$html" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Abstract" mode="html" priority="1">
      <xsl:variable name="html">
         <!-- Contents of element -->
         <xsl:apply-templates select="*" mode="html" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'#text'" />
         <xsl:with-param name="value" select="$html" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Description" mode="html" priority="1">
      <xsl:variable name="html">
         <!-- Contents of element -->
         <xsl:apply-templates select=".//heading | .//p" mode="html" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'#text'" />
         <xsl:with-param name="value" select="$html" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="Claim" mode="html" priority="1">
      <xsl:variable name="html">
         <!-- Contents of element -->
         <xsl:apply-templates select="p" mode="text" />
      </xsl:variable>

      <xsl:call-template name="create-json-field">
         <xsl:with-param name="name" select="'#text'" />
         <xsl:with-param name="value" select="$html" />
      </xsl:call-template>
   </xsl:template>
   <xsl:template match="heading" mode="html" priority="1">
      <xsl:variable name="content">
         <xsl:apply-templates select="self::*" />
      </xsl:variable>

      <!--<xsl:apply-templates select="self::*" mode="text" />-->
      <xsl:apply-templates select="exsl:node-set($content)/*" mode="text" />
   </xsl:template>
   <xsl:template match="p" mode="html" priority="1">
      <xsl:variable name="content">
         <xsl:apply-templates select="self::*" />
      </xsl:variable>

      <!--<xsl:apply-templates select="self::*" mode="text" />-->
      <xsl:apply-templates select="exsl:node-set($content)/*" mode="text" />
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="node()" mode="text">
      <!-- Node (comment/processing-instruction) not supported, replace it with a space -->
      <xsl:text> </xsl:text>
   </xsl:template>
   <xsl:template match="@*" mode="text">
      <xsl:text> </xsl:text>
      <xsl:value-of select="name()" />
      <xsl:text>=\"</xsl:text>
      <xsl:value-of select="." />
      <xsl:text>\"</xsl:text>
   </xsl:template>
   <xsl:template match="text()" mode="text" priority="1">
      <xsl:variable name="text" select="translate(., '&#13;&#10;&#09;', '')" />

      <!--<xsl:call-template name="create-json-string">-->
      <!-- Remove CR, LF and TAB characters -->
      <!--<xsl:with-param name="value" select="$text" />-->
      <!--</xsl:call-template>-->
	  <xsl:call-template name="escapeJSON">
		  <xsl:with-param name="text" select="$text" />
	  </xsl:call-template>
      <!--xsl:value-of select="$text" /-->
   </xsl:template>
   <xsl:template match="*" mode="text" priority="2">
      <!-- Opening tag (with attributes) -->
      <xsl:text>&lt;</xsl:text>
      <xsl:value-of select="name()" />
      <xsl:apply-templates select="@*" mode="text" />
      <xsl:text>&gt;</xsl:text>

      <!-- Contents of element -->
      <xsl:apply-templates select="node()" mode="text" />

      <!-- Closing tag -->
      <xsl:text>&lt;/</xsl:text>
      <xsl:value-of select="name()" />
      <xsl:text>&gt;</xsl:text>
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="claim-ref" mode="text" priority="3">
      <xsl:if test="preceding-sibling::node()">
         <xsl:variable name="text" select="preceding-sibling::node()[1]" />
         <xsl:if test="string-length($text) &gt; 0">
            <xsl:if test="normalize-space(substring($text, string-length($text)))">
               <!-- Insert (missing) space before <claim-ref> element -->
               <xsl:text> </xsl:text>
            </xsl:if>
         </xsl:if>
      </xsl:if>

      <xsl:choose>
         <xsl:when test="@generated = 'true' or @source = 'generated'">
            <!-- Copy generated claim-reference, and skip original claim-reference -->
            <xsl:text>&lt;</xsl:text>
            <xsl:value-of select="name()" />
            <xsl:apply-templates select="@idref" mode="text" />
            <xsl:text>&gt;</xsl:text>

            <!-- Contents of element -->
            <xsl:apply-templates select="node()" mode="text" />

            <!-- Closing tag -->
            <xsl:text>&lt;/</xsl:text>
            <xsl:value-of select="name()" />
            <xsl:text>&gt;</xsl:text>
         </xsl:when>
         <xsl:otherwise>
            <!-- Skip original claim-reference (it is most likely not a valid claim-reference), copy contents only -->
            <xsl:apply-templates select="node()" mode="html" />
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <xsl:template match="claim-ref/claim-ref" mode="text" priority="4">
      <!-- Skip original claim-reference, copy contents only -->
      <xsl:apply-templates select="node()" mode="text" />
   </xsl:template>
   <!-- ───────────────────────────────────────────────────────────────────────────────────────────────── -->
   <xsl:template match="tables" mode="text2" priority="3">
      <xsl:apply-templates select="table" mode="text" />
   </xsl:template>
   <xsl:template match="table" mode="text2" priority="3">
      <xsl:text>&lt;</xsl:text>
      <xsl:value-of select="name()" />
      <xsl:apply-templates select="@frame" mode="text" />
      <xsl:text>&gt;</xsl:text>

      <!-- Contents of element -->
      <xsl:apply-templates select="node()" mode="text" />

      <!-- Closing tag -->
      <xsl:text>&lt;/</xsl:text>
      <xsl:value-of select="name()" />
      <xsl:text>&gt;</xsl:text>
   </xsl:template>
   <xsl:template match="table/@frame" mode="text2" priority="1">
      <xsl:text> </xsl:text>

   </xsl:template>
   <xsl:template match="tgroup" mode="text2" priority="3">
   </xsl:template>
   <xsl:template match="tbody" mode="text2" priority="3">
   </xsl:template>
   <xsl:template match="row" mode="text2" priority="3">
      <xsl:text>&lt;</xsl:text>
      <xsl:text>tr</xsl:text>
      <xsl:apply-templates select="@*" mode="text" />
      <xsl:text>&gt;</xsl:text>

      <!-- Contents of element -->
      <xsl:apply-templates select="node()" mode="text" />

      <!-- Closing tag -->
      <xsl:text>&lt;/</xsl:text>
      <xsl:text>tr</xsl:text>
      <xsl:text>&gt;</xsl:text>
   </xsl:template>
   <xsl:template match="entry" mode="text2" priority="3">
      <xsl:text>&lt;</xsl:text>
      <xsl:text>td</xsl:text>
      <xsl:apply-templates select="@*" mode="text" />
      <xsl:text>&gt;</xsl:text>

      <!-- Contents of element -->
      <xsl:apply-templates select="node()" mode="text" />

      <!-- Closing tag -->
      <xsl:text>&lt;/</xsl:text>
      <xsl:text>td</xsl:text>
      <xsl:text>&gt;</xsl:text>
   </xsl:template>
   <xsl:template match="table/title" mode="text" priority="3">
      <xsl:text>&lt;caption&gt;</xsl:text>

      <!-- Contents of element -->
      <xsl:apply-templates select="node()" mode="text" />

      <!-- Closing tag -->
      <xsl:text>&lt;/caption&gt;</xsl:text>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Common Templates                                                                         ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->



   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ BEGIN: Named Function Templates                                                               ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->
   <xsl:template name="create-json-object">
      <xsl:param name="nodes" />

      <json>
         <xsl:text xml:space="preserve" disable-output-escaping="yes">{</xsl:text>

         <xsl:for-each select="$nodes">
            <xsl:if test="position() &gt; 1">
               <xsl:text xml:space="preserve" disable-output-escaping="yes">,</xsl:text>
            </xsl:if>

            <xsl:text xml:space="preserve" disable-output-escaping="yes">&#13;</xsl:text>
            <xsl:value-of select="." />
         </xsl:for-each>

         <xsl:text xml:space="preserve" disable-output-escaping="yes">&#13;</xsl:text>
         <xsl:text xml:space="preserve" disable-output-escaping="yes">}</xsl:text>
      </json>
   </xsl:template>

   <xsl:template name="create-json-element">
      <xsl:param name="name" />
      <xsl:param name="nodes" />

      <xsl:variable name="object">
         <xsl:call-template name="create-json-object">
            <xsl:with-param name="nodes" select="$nodes" />
         </xsl:call-template>
      </xsl:variable>

      <json>
         <xsl:text xml:space="preserve" disable-output-escaping="yes">"</xsl:text>
         <xsl:value-of select="$name" />
         <xsl:text xml:space="preserve" disable-output-escaping="yes">":</xsl:text>

         <xsl:for-each select="exsl:node-set($object)/*">
            <xsl:if test="position() &gt; 1">
               <xsl:text xml:space="preserve" disable-output-escaping="yes">,</xsl:text>
            </xsl:if>

            <xsl:value-of select="." />
         </xsl:for-each>
      </json>
   </xsl:template>

   <xsl:template name="create-json-array">
      <xsl:param name="name" />
      <xsl:param name="nodes" />

      <json>
         <xsl:text xml:space="preserve" disable-output-escaping="yes">"</xsl:text>
         <xsl:value-of select="$name" />
         <xsl:text xml:space="preserve" disable-output-escaping="yes">":</xsl:text>
         <xsl:text xml:space="preserve" disable-output-escaping="yes">[</xsl:text>

         <xsl:for-each select="$nodes">
            <xsl:if test="position() &gt; 1">
               <xsl:text xml:space="preserve" disable-output-escaping="yes">,</xsl:text>
            </xsl:if>

            <xsl:value-of select="." />
         </xsl:for-each>

         <xsl:text xml:space="preserve" disable-output-escaping="yes">&#13;</xsl:text>
         <xsl:text xml:space="preserve" disable-output-escaping="yes">]</xsl:text>
      </json>
   </xsl:template>

   <xsl:template name="create-json-field">
      <xsl:param name="name" />
      <xsl:param name="value" />

      <json>
         <xsl:value-of select="concat('&#34;', $name, '&#34;:&#34;', $value, '&#34;')" />
      </json>
   </xsl:template>

   <xsl:template name="create-json-date-field">
      <xsl:param name="name" />
      <xsl:param name="value" />

      <json>
         <xsl:text xml:space="preserve" disable-output-escaping="yes">"</xsl:text>
         <xsl:value-of select="$name" />
         <xsl:text xml:space="preserve" disable-output-escaping="yes">":</xsl:text>

         <xsl:text xml:space="preserve" disable-output-escaping="yes">"</xsl:text>
         <xsl:value-of select="substring($value, 1, 4)" />
         <xsl:text>-</xsl:text>
         <xsl:value-of select="substring($value, 5, 2)" />
         <xsl:text>-</xsl:text>
         <xsl:value-of select="substring($value, 7, 2)" />
         <xsl:text xml:space="preserve" disable-output-escaping="yes">"</xsl:text>
      </json>
   </xsl:template>
   <xsl:template name="create-json-time-field">
      <xsl:param name="name" />
      <xsl:param name="value" />

      <json>
         <xsl:text xml:space="preserve" disable-output-escaping="yes">"</xsl:text>
         <xsl:value-of select="$name" />
         <xsl:text xml:space="preserve" disable-output-escaping="yes">":</xsl:text>

         <xsl:text xml:space="preserve" disable-output-escaping="yes">"</xsl:text>
         <xsl:value-of select="substring($value, 1, 2)" />
         <xsl:text>:</xsl:text>
         <xsl:value-of select="substring($value, 3, 2)" />
         <xsl:text>:</xsl:text>
         <xsl:value-of select="substring($value, 5, 2)" />
         <xsl:text>.</xsl:text>
         <xsl:value-of select="substring($value, 7, 3)" />
         <xsl:text xml:space="preserve" disable-output-escaping="yes">"</xsl:text>
      </json>
   </xsl:template>
   
   <xsl:template name="escapeJSON">
      <xsl:param name="text" />
      
      <xsl:variable name="escaped">
         <xsl:call-template name="escapeBack">
            <xsl:with-param name="text" select="$text" />         
         </xsl:call-template>
      </xsl:variable>
      
      <xsl:call-template name="escapeQuote">
         <xsl:with-param name="text" select="$escaped" />
      </xsl:call-template>
   </xsl:template>
   
   <xsl:template name="escapeBack">
      <xsl:param name="text" select="."/>

      <xsl:if test="string-length($text) &gt; 0">
         <xsl:value-of select="substring-before(concat($text, '\'), '\')" />

         <xsl:if test="contains($text, '\')">
            <xsl:text>\\</xsl:text>

            <xsl:call-template name="escapeBack">
               <xsl:with-param name="text" select="substring-after($text, '\')" />
            </xsl:call-template>
         </xsl:if>
      </xsl:if>
   </xsl:template>
   
   <xsl:template name="escapeQuote">
      <xsl:param name="text" select="."/>

      <xsl:if test="string-length($text) &gt; 0">
         <xsl:value-of select="substring-before(concat($text, '&quot;'), '&quot;')" />

         <xsl:if test="contains($text, '&quot;')">
            <xsl:text>\"</xsl:text>

            <xsl:call-template name="escapeQuote">
               <xsl:with-param name="text" select="substring-after($text, '&quot;')" />
            </xsl:call-template>
         </xsl:if>
      </xsl:if>
   </xsl:template>
   <!-- ╔═══════════════════════════════════════════════════════════════════════════════════════════════╗ -->
   <!-- ║ END: Named Function Templates                                                                 ║ -->
   <!-- ╚═══════════════════════════════════════════════════════════════════════════════════════════════╝ -->

</xsl:stylesheet>

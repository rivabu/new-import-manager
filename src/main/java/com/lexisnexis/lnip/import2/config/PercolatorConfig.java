package com.lexisnexis.lnip.import2.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.AbstractConnectionFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PercolatorConfig {



	/** The address. */
	@Value("${percolator.rabbit.host}")
	String host;
	
	/** The port. */
	@Value("${percolator.rabbit.port}")
	int port;

	/** The queue name. */
	@Value("${percolator.rabbit.username}")
	String username;

	/** The queue name. */
	@Value("${percolator.rabbit.password}")
	String password;
	
	/** The queue name. */
	@Value("${percolator.rabbit.queue}")
	String queueName;
	
	
	
	@Bean(name = "rabbitAdminPercolator")
	public RabbitAdmin admim() {
		return new RabbitAdmin(connectionFactory());
	}

	public ConnectionFactory connectionFactory() {
		AbstractConnectionFactory connectionFactory = new CachingConnectionFactory(host);
		connectionFactory.setPort(port);
		connectionFactory.setUsername(username);
		connectionFactory.setPassword(password);
		return connectionFactory;
	}
	
	
	@Bean(name = "percolatoreTemplate")
	public RabbitTemplate rabbitTemplate() {
		RabbitTemplate template = new RabbitTemplate(connectionFactory());
		//The routing key is set to the name of the queue by the broker for the default exchange.
		template.setRoutingKey(this.queueName);
		//Where we will synchronously receive messages from
		template.setQueue(this.queueName);
		return template;
	}

	@Bean(name = "percolatorQueue")
	// Every queue is bound to the default direct exchange
	public Queue getQueue() {
		return new Queue(this.queueName);
	}


}

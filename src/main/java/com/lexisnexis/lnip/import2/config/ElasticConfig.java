package com.lexisnexis.lnip.import2.config;

import java.util.Properties;

import org.elasticsearch.client.Client;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.lexisnexis.lnip.import2.services.ElasticSearchService;

import fr.pilato.spring.elasticsearch.ElasticsearchTransportClientFactoryBean;


@Configuration
@ConfigurationProperties(prefix = "es")
public class ElasticConfig {

    @NotEmpty
    private String[] nodes;

    @NotBlank
    private String clusterName;
    
    @NotBlank
    private String index;

    @NotBlank
    private String percolatorIndex;


	@Bean
    public ElasticsearchTransportClientFactoryBean esClient() {
        ElasticsearchTransportClientFactoryBean clientFactory = new ElasticsearchTransportClientFactoryBean();
        Properties p = new Properties();
        p.put("cluster.name", clusterName);
        clientFactory.setProperties(p);
        clientFactory.setEsNodes(nodes);
        return clientFactory;
    }
    
    @Bean
    public ElasticSearchService elasticSearchService(Client esClient, RabbitTemplate rabbitTemplate) {
    	return new ElasticSearchService(esClient, index, percolatorIndex, rabbitTemplate);
    }

    public String[] getNodes() {
        return nodes;
    }

    public void setNodes(String[] nodes) {
        this.nodes = nodes;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

    public String getPercolatorIndex() {
		return percolatorIndex;
	}

	public void setPercolatorIndex(String percolatorIndex) {
		this.percolatorIndex = percolatorIndex;
	}

}

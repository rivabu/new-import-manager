package com.lexisnexis.lnip.import2.config;

import javax.validation.Valid;

import org.hibernate.validator.constraints.NotBlank;
import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.BlobStoreContext;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "storage")
public class SwiftStorage {

    @Valid
    private SwiftParams swift;

    private int queueSize = 128;
    private int minPoolSize = 8;
    private int maxPoolSize = 16;

    @Bean
    public BlobStore objectStore() {
        BlobStoreContext context = ContextBuilder.newBuilder(swift.getProvider())
                .endpoint(swift.getEndPoint())
                .credentials(swift.getIdentity(), swift.credentials)
                .buildView(BlobStoreContext.class);
        BlobStore blobStore = context.getBlobStore();
        return blobStore;
    }

    public int getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(int queueSize) {
        this.queueSize = queueSize;
    }

    public int getMinPoolSize() {
        return minPoolSize;
    }

    public void setMinPoolSize(int minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    public SwiftParams getSwift() {
        return swift;
    }

    public void setSwift(SwiftParams swift) {
        this.swift = swift;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

	public static class SwiftParams {

        @NotBlank
        private String endPoint;

        @NotBlank
        private String identity;

        @NotBlank
        private String credentials;

        private String provider = "openstack-swift";

        public String getEndPoint() {
            return endPoint;
        }

        public void setEndPoint(String endPoint) {
            this.endPoint = endPoint;
        }

        public String getIdentity() {
            return identity;
        }

        public void setIdentity(String identity) {
            this.identity = identity;
        }

        public String getCredentials() {
            return credentials;
        }

        public void setCredentials(String credentials) {
            this.credentials = credentials;
        }

        public String getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }
    }

}

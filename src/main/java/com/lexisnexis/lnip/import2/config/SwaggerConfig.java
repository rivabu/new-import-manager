package com.lexisnexis.lnip.import2.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@ConditionalOnWebApplication
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    @ConditionalOnWebApplication
    public Docket api() {
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("Backoffice connector")
                .description("This is a backend service for communicating with the backoffice (BPS)")
                .version("1.0")
                .build();

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .paths(PathSelectors.regex("/user/.*"))
                .build();
    }
}

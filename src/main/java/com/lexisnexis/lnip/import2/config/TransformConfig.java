package com.lexisnexis.lnip.import2.config;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.Templates;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.searchtechnologies.datamodel.PatentDocument;

@Configuration
public class TransformConfig {

	private static final String DISPLAY_STYLESHEET = "/display-json.xslt";
	private static final String SEARCH_STYLESHEET = "/DataModelMapper.xslt";

	private static final Logger logger = LoggerFactory.getLogger(TransformConfig.class);

	@Bean
	public Templates displayTemplates() throws TransformerConfigurationException {
		return getTemplates(DISPLAY_STYLESHEET);
	}

	@Bean
	public Templates searchTemplates() throws TransformerConfigurationException {
		return getTemplates(SEARCH_STYLESHEET);
	}

	@Bean
	public JAXBContext jaxbContext() throws JAXBException {
		return JAXBContext.newInstance(PatentDocument.class);
	}

	public Templates getTemplates(String stylesheetName) throws TransformerConfigurationException {
		InputStream stylesheet = getClass().getResourceAsStream(stylesheetName);
		if (stylesheet == null) {
			throw new IllegalStateException(
					String.format("Transformation stylesheet %s was not found on the classpath.", stylesheetName));
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setErrorListener(new ErrorListener() {

			@Override
			public void error(TransformerException exception) {
				logger.error(exception.toString());
			}

			@Override
			public void fatalError(TransformerException exception) {
				logger.error(exception.toString());
			}

			@Override
			public void warning(TransformerException exception) {
				// handle warnings as well if you want them
				logger.warn(exception.toString());
			}
		});
		return transformerFactory.newTemplates(new StreamSource(stylesheet));
	}

}

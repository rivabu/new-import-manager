package com.lexisnexis.lnip.import2.web;

import static com.lexisnexis.lnip.CorrelationId.CORRELATION_ID;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lexisnexis.lnip.import2.model.DocMessage;
import com.lexisnexis.lnip.import2.model.RequestResult;
import com.lexisnexis.lnip.import2.model.RequestResult.RESULT_CODE;
import com.lexisnexis.lnip.import2.services.ElasticSearchService;
import com.lexisnexis.lnip.import2.services.Metrics;
import com.lexisnexis.lnip.import2.services.StorageService;
import com.lexisnexis.lnip.import2.services.XSLTProcessor;
import com.searchtechnologies.datamodel.PatentDocument;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api")
@Api(value = "api")
public class InputController {
	private static final Logger LOG = LoggerFactory.getLogger(InputController.class);

	@Autowired
	private ElasticSearchService elasticSearchService;

	@Autowired
	private StorageService storageService;

	@Autowired
	private XSLTProcessor xSLTProcessor;

	@Autowired
	private Metrics metrics;

	private long count = 0;

	@ApiOperation(httpMethod = "POST", value = "Handle file endpoint", notes = "Proces document", response = ResponseEntity.class)
	@RequestMapping(value = "/v1/file/{filename:.+}", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_XML_VALUE }, consumes = { MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<RequestResult> handleFile(HttpServletRequest request, @PathVariable String filename)
			throws Throwable {
		count++;
		long time = System.currentTimeMillis();
		LOG.debug("received {} ({})", filename, count);
		BufferedInputStream fileString = getPostBody(request, filename);
		long a = System.currentTimeMillis() - time;
		LOG.debug("get bytes in {}", a);
		MDC.put(CORRELATION_ID, filename);
		ResponseEntity<RequestResult> entity = new ResponseEntity<RequestResult>(HttpStatus.OK);
		if (fileString.available() > 0) {
			String publicationNumber = filename.substring(0, filename.lastIndexOf('.'));
			DocMessage doc = new DocMessage(publicationNumber, IOUtils.toByteArray(fileString));
			long millis = System.currentTimeMillis();
			PatentDocument patentDocument = xSLTProcessor.processSearchAndMarshall(doc.getDocBody());
			metrics.updateTransformXML(System.currentTimeMillis() - millis);
			patentDocument.setIndexDateTime(new Date());
			LOG.debug("patentDoc created");
			if (patentDocument.getPublication() != null && patentDocument.getPublication().getPublicationId() != null) {
				doc.setId(patentDocument.getPublication().getPublicationId());
				patentDocument.getPublication().setPublicationId(doc.getPublicationId());

				List<CompletableFuture<Boolean>> futures = new ArrayList<>();
				futures.add(storageService.handleXML(doc));
				futures.add(storageService.handleDisplayJson(doc));
				futures.add(elasticSearchService.addDoc(patentDocument, doc.getId(),
						doc.getPublicationId()));

				List<Boolean> resultList = futures.stream().map(CompletableFuture::join).collect(Collectors.toList());

				RequestResult result = new RequestResult();
				result.setFileName(doc.getPublicationId());
				if (resultList.contains(false)) {
					result.setRes(RESULT_CODE.FAIL);
					entity = new ResponseEntity<RequestResult>(result, HttpStatus.INTERNAL_SERVER_ERROR);
				} else {
					result.setRes(RESULT_CODE.OK);
					entity = new ResponseEntity<RequestResult>(result, HttpStatus.OK);
				}
			}
		} else {
			entity = new ResponseEntity<RequestResult>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		time = System.currentTimeMillis() - time;
		LOG.debug("{} finished", time);
		metrics.updateProcessingTime(time);
		return entity;
	}

	public CompletableFuture<Void> execAll(DocMessage doc, PatentDocument patentDocument) {
		return (CompletableFuture.<Void> allOf(CompletableFuture.runAsync(() -> (storageService).handleXML(doc)),
				CompletableFuture.runAsync(() -> (storageService).handleDisplayJson(doc)), CompletableFuture.runAsync(
						() -> (elasticSearchService).addDoc(patentDocument, doc.getId(), doc.getPublicationId()))));
	}

	private BufferedInputStream getPostBody(final HttpServletRequest request, final String fileName) throws Exception {
		return new BufferedInputStream(request.getInputStream());
	}

}

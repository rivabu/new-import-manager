package com.lexisnexis.lnip.import2.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import javax.annotation.Resource;

import org.jclouds.blobstore.domain.Blob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.io.ByteSource;
import com.lexisnexis.lnip.import2.model.DocMessage;

@Component
public class StorageService {

	private static final Logger LOG = LoggerFactory.getLogger(StorageService.class);

	@Resource
	SwiftConnector swiftConnector;

	@Autowired
	private XSLTProcessor xSLTProcessor;

	@Autowired
	private Metrics metrics;

	@Autowired
	private ExecutorService executor;

	public CompletableFuture<Boolean> handleXML(DocMessage doc) {
		return CompletableFuture.supplyAsync(() -> {
			return storeXMLInSwift(doc);
		}, executor);
	}

	public CompletableFuture<Boolean> handleDisplayJson(DocMessage doc) {
		return CompletableFuture.supplyAsync(() -> {
			boolean returnValue = false;
			try {
				ByteArrayOutputStream json = transformToDisplayJson(doc);
				storeJsonInSwift(doc.getId() + ".json", json, "json");
				returnValue = true;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return returnValue;
		}, executor);
	}

	private ByteArrayOutputStream transformToDisplayJson(DocMessage doc)
			throws Exception, UnsupportedEncodingException {
		long millis = System.currentTimeMillis();
		String content = new String(doc.getDocBody()).replaceAll("&", "&amp;");
		// String content = new String(doc.getDocBody()).replaceAll("&",
		// "&amp;");
		if (!content.startsWith("<")) {
			LOG.error(content.substring(0, 10));
			content = content.substring(content.indexOf("<"));
		}
		ByteArrayOutputStream json = xSLTProcessor.processDisplay(content.getBytes());
		metrics.updateTransformJSON(System.currentTimeMillis() - millis);
		return json;
	}

	private boolean storeXMLInSwift(DocMessage doc) {
		boolean returnValue = false;
		try {
			long millis = System.currentTimeMillis();

			ByteSource source = ByteSource.wrap(doc.getDocBody());
			String filename = doc.getId() + ".xml";
			Blob blob = null;

			blob = swiftConnector.blobBuilder(doc.getId() + ".xml").payload(source).contentLength(source.size())
					.contentType("application/xml").build();
			swiftConnector.putBlob("xml", blob);
			LOG.debug("stored in swift: {}/{}", filename);
			metrics.updateStoreXML(System.currentTimeMillis() - millis);
			returnValue = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnValue;
	}

	private void storeJsonInSwift(String filename, ByteArrayOutputStream data, String bucket) throws IOException {
		long millis = System.currentTimeMillis();

		ByteSource source = ByteSource.wrap(data.toByteArray());
		Blob blob = swiftConnector.blobBuilder(filename).payload(source).contentLength(source.size())
				.contentType("application/json").build();
		swiftConnector.putBlob(bucket, blob);
		LOG.debug("stored in swift: {}/{}", bucket, filename);
		metrics.updateStoreJSON(System.currentTimeMillis() - millis);
	}

}

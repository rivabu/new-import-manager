package com.lexisnexis.lnip.import2.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Component;

@Component
public class Metrics {
    
    GaugeService gaugeService;
    
    private long count;
    private double sumProcessingTime;
    private double sumTransformXML;
    private double sumTransformJSON;
    private double sumStoreXML;
    private double sumStoreJSON;
    private double sumStoreElastic;
    private double sumPercolate;

    @Autowired
    public Metrics(GaugeService gaugeService) {
        this.gaugeService = gaugeService;
        this.count = 1;
    }

    public synchronized void updateProcessingTime(long number) {
    	sumProcessingTime = sumProcessingTime + number;
        gaugeService.submit("avr.processingtime", sumProcessingTime/count);
    	count++;
    }
    
    public synchronized void updateTransformXML(long number) {
    	sumTransformXML = sumTransformXML + number;
        gaugeService.submit("avr.transformXML", sumTransformXML/count);
    }

    public synchronized void updateTransformJSON(long number) {
    	sumTransformJSON = sumTransformJSON + number;
        gaugeService.submit("avr.transformJSON", sumTransformJSON/count);
    }

    public synchronized void updateStoreXML(long number) {
    	sumStoreXML = sumStoreXML + number;
        gaugeService.submit("avr.storeXML", sumStoreXML/count);
    }

    public synchronized void updateStoreJSON(long number) {
    	sumStoreJSON = sumStoreJSON + number;
        gaugeService.submit("avr.storeJSON", sumStoreJSON/count);
    }

    public synchronized void updateStoreElastic(long number) {
    	sumStoreElastic = sumStoreElastic + number;
        gaugeService.submit("avr.storeElastic", sumStoreElastic/count);
    }

    public synchronized void updatePercolate(long number) {
    	sumPercolate = sumPercolate + number;
        gaugeService.submit("avr.percolate", sumPercolate/count);
    }
}

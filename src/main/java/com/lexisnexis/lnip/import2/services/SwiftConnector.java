package com.lexisnexis.lnip.import2.services;

import org.jclouds.blobstore.BlobStore;
import org.jclouds.blobstore.domain.Blob;
import org.jclouds.blobstore.domain.BlobBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SwiftConnector {

	private static final Logger LOG = LoggerFactory.getLogger(SwiftConnector.class);

	@Autowired
	private BlobStore objectStore;

	public void putBlob(String container, Blob blob) {
		long time = System.currentTimeMillis();

		try {
			objectStore.putBlob(container, blob);
			time = System.currentTimeMillis() - time;
		} catch (RuntimeException e) {
			time = System.currentTimeMillis() - time;
			LOG.debug("SWIFT-DURATION: {}", time);
			throw e;
		}
		LOG.debug("SWIFT-DURATION: {}", time);
	}

	public BlobBuilder blobBuilder(String name) {
		return objectStore.blobBuilder(name);
	}

}

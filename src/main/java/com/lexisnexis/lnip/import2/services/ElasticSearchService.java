package com.lexisnexis.lnip.import2.services;

import java.io.ByteArrayOutputStream;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import org.elasticsearch.action.ActionFuture;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.percolate.MultiPercolateRequestBuilder;
import org.elasticsearch.action.percolate.MultiPercolateResponse;
import org.elasticsearch.action.percolate.PercolateRequest;
import org.elasticsearch.action.percolate.PercolateRequestBuilder;
import org.elasticsearch.action.percolate.PercolateResponse;
import org.elasticsearch.client.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lexisnexis.lnip.import2.model.AlertMessage;
import com.lexisnexis.lnip.import2.model.DocMessage;
import com.lexisnexis.lnip.import2.model.Match;
import com.searchtechnologies.datamodel.PatentDocument;

public class ElasticSearchService {

	ObjectMapper mapper = new ObjectMapper();

	private Client esClient;

	private String index;

	private String percolatorIndex;

	private AmqpTemplate rabbitTemplate;

	@Autowired
	private Metrics metrics;

	@Autowired
	private ExecutorService executor;

	public ElasticSearchService(Client esClient, String index, String percolatorIndex, AmqpTemplate rabbitTemplate) {
		super();
		this.esClient = esClient;
		this.index = index;
		this.percolatorIndex = percolatorIndex;
		this.rabbitTemplate = rabbitTemplate;
	}

	private static final Logger LOG = LoggerFactory.getLogger(ElasticSearchService.class);


	public CompletableFuture<Boolean> addDoc(PatentDocument patentDocument, String id, String publicationId) {
		return CompletableFuture.supplyAsync(() -> {
			boolean returnValue = false;
			long millis = System.currentTimeMillis();
			try {
				ByteArrayOutputStream json = objectToJson(patentDocument);
				IndexResponse response = esClient.prepareIndex(index, "univentio_patent_document", id)
						.setSource(json.toByteArray()).get();
				long executionTime = System.currentTimeMillis() - millis;
				metrics.updateStoreElastic(executionTime);
				LOG.debug("stored in elastic in {}", executionTime);

				percolateDocument(id, publicationId);
				returnValue = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
			return returnValue;
		}, executor);
	}

	private ByteArrayOutputStream objectToJson(PatentDocument patentDocument) throws Exception {
		ByteArrayOutputStream objectBytes = new ByteArrayOutputStream();
		mapper.writeValue(objectBytes, patentDocument);
		return objectBytes;
	}

	public void percolateDocument(String id, String publicationId) {
		long millis = System.currentTimeMillis();

		PercolateRequest pr = new PercolateRequest().indices(percolatorIndex).documentType("univentio_patent_document");
		pr.getRequest(new GetRequest(index, "univentio_patent_document", id));
		ActionFuture<PercolateResponse> future = esClient.percolate(pr);
		PercolateResponse percResponse = future.actionGet();

		AlertMessage alertMessage = new AlertMessage();
		Match match = new Match(id, publicationId);
		for (PercolateResponse.Match matchedQuery : percResponse) {
			match.addId(matchedQuery.getId().string());
		}
		if (match.getIds().size() > 0) {
			alertMessage.addMatch(match);
		}

		if (alertMessage.getMatches().size() > 0) {
			try {
				// if there is at least one match then send the alarm
				ObjectMapper mapper = new ObjectMapper();
				ByteArrayOutputStream objectBytes = new ByteArrayOutputStream();
				mapper.writeValue(objectBytes, alertMessage);

				String objectString = new String(objectBytes.toByteArray(), "UTF-8");
				rabbitTemplate.convertAndSend(objectString);
				LOG.debug("Percolator alert message was sent.");
			} catch (Exception e) {
				LOG.error("Percolator alert message was not sent.", e);
			}
		}
		long executionTime = System.currentTimeMillis() - millis;
		metrics.updatePercolate(executionTime);
		LOG.debug("updatePercolate in {}", executionTime);

	}
}

package com.lexisnexis.lnip.import2.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.searchtechnologies.datamodel.PatentDocument;

@Component
public class XSLTProcessor {

	@Autowired
	JAXBContext jaxbContext;

	@Autowired
	Templates searchTemplates;
	
	@Autowired
	Templates displayTemplates;
	
	private ByteArrayOutputStream transform(final InputStream stream, boolean display)
			throws TransformerException, ParserConfigurationException, UnsupportedEncodingException, JAXBException {

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(outputStream);
		Source source = new StreamSource(stream);
		if (display) {
			displayTemplates.newTransformer().transform(source, result);
		} else {
			searchTemplates.newTransformer().transform(source, result);
		}
		return outputStream;

	}

	private PatentDocument marshall(ByteArrayOutputStream outputStream) throws JAXBException {
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		PatentDocument patentDoc = (PatentDocument) jaxbUnmarshaller
				.unmarshal(new ByteArrayInputStream(outputStream.toByteArray()));
		return patentDoc;
	}

	public PatentDocument processSearchAndMarshall(byte[] content) throws Exception {
		ByteArrayOutputStream outputStream;
		outputStream = transform(new ByteArrayInputStream(content), false);
		return marshall(outputStream);
	}

	public ByteArrayOutputStream processDisplay(byte[] content) throws Exception {

		ByteArrayOutputStream outputStream = transform(new ByteArrayInputStream(content), true);
//		String out = new String(outputStream.toByteArray(), "UTF-8");
		return outputStream;
	}
}

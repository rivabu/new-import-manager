/**
 * 
 */
package com.lexisnexis.lnip.import2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AlertMessage implements Serializable {
    private static final long serialVersionUID = -155886161700302818L;

    @JsonProperty("alertType")
    private String alertType;
    @JsonProperty("matches")
    private List<Match> matches;

    public AlertMessage(){
        alertType = "querymatch";
        matches = new ArrayList<>();
    }

    public String getAlertType() {
        return alertType;
    }

    public void addMatch(Match match){
        matches.add(match);
    }

    public List<Match> getMatches() {
        return matches;
    }

    @Override
    public String toString() {
        return "AlertMessage{" +
                "alertType='" + alertType + '\'' +
                ", matches=" + matches +
                '}';
    }

}

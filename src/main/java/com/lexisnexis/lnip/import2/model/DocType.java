package com.lexisnexis.lnip.import2.model;

public enum DocType {
    XML("XML"),
    BIBLIO("BIBLIO"),
    CLAIMS("CLAIMS"),
    DESCRIPTION("DESCR"),
	JSON("JSON");
    
    private String suffix;

    DocType(String suffix) {
        this.suffix = suffix;
    }

    public String toString() {
        return suffix;
    }

    public String toFileName(String prefix) {
        return String.format("%s_%s.json", prefix, suffix);
    }
}

package com.lexisnexis.lnip.import2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Match implements Serializable {
    private static final long serialVersionUID = -2147222468776728604L;

    @JsonProperty("docId")
    private String docId;

    @JsonProperty("publId")
    private String publId;

    @JsonProperty("ids")
    private List<String> ids;

    public Match() {
        ids = new ArrayList<>();
    }

    public Match(String docId, String publId) {
        this();
        this.docId = docId;
        this.publId = publId;
    }

    public void addId(String id) {
        this.ids.add(id);
    }

    public List<String> getIds() {
        return ids;
    }

    @Override
    public String toString() {
        return "Match{" +
                "docId='" + docId + ',' +
                "publId='" + publId + '\'' +
                ", ids=" + ids +
                '}';
    }

}

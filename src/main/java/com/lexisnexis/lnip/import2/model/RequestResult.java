/**
 * 
 */
package com.lexisnexis.lnip.import2.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name="document-push-result")
public class RequestResult implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = -8822047275444048049L;
    @XmlEnum
    public enum RESULT_CODE{
        OK, FAIL
    }
    @JsonProperty("Result")
    RESULT_CODE res;
    @JsonProperty("FileName")
    String fileName;
    @JsonProperty("ErrorMessage")
    String errorMessage;
    
    /**
     * @return the res
     */
    public RESULT_CODE getRes() {
        return res;
    }
    /**
     * @param res the res to set
     */
    @XmlElement( name = "Result" )
    public void setRes(RESULT_CODE res) {
        this.res = res;
    }
    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }
    /**
     * @param fileName the fileName to set
     */
    @XmlElement( name = "FileName" )
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }
    /**
     * @param errorMessage the errorMessage to set
     */
    @XmlElement( name = "ErrorMessage")
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    

    
}

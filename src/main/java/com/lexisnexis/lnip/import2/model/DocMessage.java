/**
 * 
 */
package com.lexisnexis.lnip.import2.model;

import java.io.Serializable;

public class DocMessage implements Serializable {
    public DocMessage(String publicationId, byte[] docBody) {
		super();
		this.publicationId = publicationId;
		this.docBody = docBody;
	}
	/**
     * 
     */
    private static final long serialVersionUID = 8641694235852709388L;
    private String publicationId;
    private byte[] docBody;
    private String id;
    /**
     * @return the publicationNumber
     */
    public String getPublicationId() {
        return publicationId;
    }
    /**
     * @param publicationNumber the publicationNumber to set
     */
    public void setPublicationId(String publicationId) {
        this.publicationId = publicationId;
    }
    /**
     * @return the docBody
     */
    public byte[] getDocBody() {
        return docBody;
    }
    /**
     * @param docBody the docBody to set
     */
    public void setDocBody(byte[] docBody) {
        this.docBody = docBody;
    }
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
